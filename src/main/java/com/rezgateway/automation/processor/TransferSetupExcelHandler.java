package com.rezgateway.automation.processor;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.Objects.SupplierSetupDetails;
import com.rezgateway.automation.Objects.TransferCancellationPolicyDetails;
import com.rezgateway.automation.Objects.TransferCityZoneConfiguration;
import com.rezgateway.automation.Objects.TransferContractInfo;
import com.rezgateway.automation.Objects.TransferLocationConfiguration;
import com.rezgateway.automation.Objects.TransferRateDetails;
import com.rezgateway.automation.Objects.TransferRouteAndVehicleInfo;
import com.rezgateway.automation.Objects.TransferSetupDetails;
import com.rezgateway.automation.Objects.TransferStandardInfo;
import com.rezgateway.automation.Objects.TransferTimeConfiguration;
import com.rezgateway.automation.Objects.TransferVehicleConfiguration;

public class TransferSetupExcelHandler extends RunnerBase{

	public TransferSetupDetails getObject(String excelPath) throws FileNotFoundException
	{
		TransferSetupDetails setupDetails = new TransferSetupDetails();
		
		ArrayList<String[]> 							standardInfo 				= readExcel(excelPath, 0);
		ArrayList<String[]> 							zoneConfig 					= readExcel(excelPath, 1);
		ArrayList<String[]> 							locationConfig 				= readExcel(excelPath, 2);
		ArrayList<String[]> 							vehicleConfig 				= readExcel(excelPath, 3);
		ArrayList<String[]> 							timeConfig 					= readExcel(excelPath, 4);
		ArrayList<String[]> 							supplierDetails				= readExcel(excelPath, 5);
		ArrayList<String[]> 							routeAndVehicleDetails 		= readExcel(excelPath, 6);
		ArrayList<String[]> 							contractDetails				= readExcel(excelPath, 7);
		ArrayList<String[]> 							cancellationPolicyDetails	= readExcel(excelPath, 8);
		ArrayList<String[]> 							rateDetails					= readExcel(excelPath, 9);
		
		
		ArrayList<TransferCityZoneConfiguration> 		zoneList 					= new ArrayList<>();
		ArrayList<TransferContractInfo> 				contractlist 				= new ArrayList<>();
		ArrayList<TransferLocationConfiguration> 		locationlist 				= new ArrayList<>();
		ArrayList<TransferStandardInfo> 				standardInfolist 			= new ArrayList<>();
		ArrayList<TransferTimeConfiguration> 			timeList 					= new ArrayList<>();
		ArrayList<TransferVehicleConfiguration> 		vehicleList 				= new ArrayList<>();
		ArrayList<SupplierSetupDetails> 				supplierlist				= new ArrayList<>();
		ArrayList<TransferRouteAndVehicleInfo> 			routeList					= new ArrayList<>();
		ArrayList<TransferContractInfo> 				contractList				= new ArrayList<>();
		ArrayList<TransferCancellationPolicyDetails> 	cancellationList			= new ArrayList<>();
		ArrayList<TransferRateDetails> 					rateList					= new ArrayList<>();
		
		for(int i = 0 ; i < cancellationPolicyDetails.size() ; i ++)
		{
			TransferCancellationPolicyDetails canPolicy = new TransferCancellationPolicyDetails();
			cancellationList.add(canPolicy.getDetails(cancellationPolicyDetails.get(i)));
		}
		
		for(int i = 0 ; i < contractDetails.size() ; i ++)
		{
			TransferContractInfo contract = new TransferContractInfo();
			contractList.add(contract.getDetails(contractDetails.get(i)));
		}
		
		for(int i = 0 ; i < routeAndVehicleDetails.size() ; i ++)
		{
			TransferRouteAndVehicleInfo route = new TransferRouteAndVehicleInfo();
			routeList.add(route.getDetails(routeAndVehicleDetails.get(i)));
		}
		
		for(int i = 0 ; i < supplierDetails.size() ; i ++)
		{
			SupplierSetupDetails supplier = new SupplierSetupDetails();
			supplierlist.add(supplier.getDetails(supplierDetails.get(i)));
		}
		
		for(int i = 0 ; i < standardInfo.size() ; i++)
		{
			TransferStandardInfo standard = new TransferStandardInfo();
			standardInfolist.add(standard.getDetails(standardInfo.get(i)));
		}
		
		for(int i = 0 ; i < zoneConfig.size() ; i++)
		{
			TransferCityZoneConfiguration zone = new TransferCityZoneConfiguration();
			zoneList.add(zone.getDetails(zoneConfig.get(i)));
		}
		
		for(int i = 0 ; i < locationConfig.size() ; i++)
		{
			TransferLocationConfiguration location = new TransferLocationConfiguration();
			locationlist.add(location.getDetails(locationConfig.get(i)));
		}
		
		for(int i = 0 ; i < vehicleConfig.size() ; i++)
		{
			TransferVehicleConfiguration vehicle = new TransferVehicleConfiguration();
			vehicleList.add(vehicle.getDetails(vehicleConfig.get(i)));
		}
		
		for(int i = 0 ; i < timeConfig.size() ; i++)
		{
			TransferTimeConfiguration time = new TransferTimeConfiguration();
			timeList.add(time.getDetails(timeConfig.get(i)));
		}
		
		for(int i = 0 ; i < contractDetails.size() ; i++)
		{
			TransferContractInfo contract = new TransferContractInfo();
			contractlist.add(contract.getDetails(contractDetails.get(i)));
		}
		
		for(int i = 0 ; i < rateDetails.size() ; i++)
		{
			TransferRateDetails rate = new TransferRateDetails();
			rateList.add(rate.getDetails(rateDetails.get(i)));
		}
		
		setupDetails.setZoneConfig(zoneList);
		setupDetails.setLocationConfig(locationlist);
		setupDetails.setStandardInfo(standardInfolist);
		setupDetails.setTimeConfig(timeList);
		setupDetails.setVehicleConfig(vehicleList);
		setupDetails.setSupplier(supplierlist);
		setupDetails.setRouteList(routeList);
		setupDetails.setContractList(contractList);
		setupDetails.setCancellationList(cancellationList);
		setupDetails.setRouteList(routeList);
		setupDetails.setRateList(rateList);
		
		return setupDetails;
	}
	
}
