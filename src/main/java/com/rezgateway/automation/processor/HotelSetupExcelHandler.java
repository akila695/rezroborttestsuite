package com.rezgateway.automation.processor;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.Objects.HotelBillingInfo;
import com.rezgateway.automation.Objects.HotelCancellationPolicyDetails;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelRoomAndInventoryInfo;
import com.rezgateway.automation.Objects.HotelRoomCombinations;
import com.rezgateway.automation.Objects.HotelSetupDetails;
import com.rezgateway.automation.Objects.HotelStandardInfo;
import com.rezgateway.automation.Objects.HotelTaxDetails;

public class HotelSetupExcelHandler extends RunnerBase{

	public HotelSetupDetails getObject(String excelPath) throws FileNotFoundException
	{
		HotelSetupDetails 								setupDetails 			= new HotelSetupDetails();
		ArrayList<String[]>								standardInfo 			= readExcel(excelPath, 0);
		ArrayList<String[]>								inventoryInfo 			= readExcel(excelPath, 1);
		ArrayList<String[]>								contractInfo 			= readExcel(excelPath, 2);
		ArrayList<String[]>								roomCombinations 		= readExcel(excelPath, 3);
		ArrayList<String[]>								taxInfo	 				= readExcel(excelPath, 4);
		ArrayList<String[]>								billingInfo 			= readExcel(excelPath, 5);
		ArrayList<String[]>								cancellationpolicyInfo 	= readExcel(excelPath, 6);
		
		ArrayList<HotelStandardInfo> 					stdInfoList 			= new ArrayList<>();
		ArrayList<HotelRoomAndInventoryInfo> 			inventoryList 			= new ArrayList<>();
		ArrayList<HotelContractInfo> 					contractlist 			= new ArrayList<>();
		ArrayList<HotelRoomCombinations> 				roomlist 				= new ArrayList<>();
		ArrayList<HotelTaxDetails> 						taxList 				= new ArrayList<>();
		ArrayList<HotelBillingInfo> 					billingList 			= new ArrayList<>();
		ArrayList<HotelCancellationPolicyDetails> 		canPolicyList 			= new ArrayList<>();
		
		for(int i = 0 ; i < standardInfo.size() ; i++)
		{
			HotelStandardInfo std = new HotelStandardInfo();
			stdInfoList.add(std.getDetails(standardInfo.get(i)));
		}
		
		for(int i = 0 ; i < inventoryInfo.size() ; i++)
		{
			HotelRoomAndInventoryInfo inv = new HotelRoomAndInventoryInfo();
			inventoryList.add(inv.getDetails(inventoryInfo.get(i)));
		}
		
		for(int i = 0 ; i < contractInfo.size() ; i++)
		{
			HotelContractInfo con = new HotelContractInfo();
			contractlist.add(con.getDetails(contractInfo.get(i)));
		}
		
		for(int i = 0 ; i < roomCombinations.size() ; i++)
		{
			HotelRoomCombinations room = new HotelRoomCombinations();
			roomlist.add(room.getDetails(roomCombinations.get(i)));
		}
		
		for(int i = 0 ; i < taxInfo.size() ; i++)
		{
			HotelTaxDetails tax = new HotelTaxDetails();
			taxList.add(tax.getDetails(taxInfo.get(i)));
		}
		
		for(int i = 0 ; i < billingInfo.size() ; i++)
		{
			HotelBillingInfo bill = new HotelBillingInfo();
			billingList.add(bill.getDetails(billingInfo.get(i)));
		}
		
		for(int i = 0 ; i < cancellationpolicyInfo.size() ; i++)
		{
			HotelCancellationPolicyDetails canDetails = new HotelCancellationPolicyDetails();
			canPolicyList.add(canDetails.getDetails(cancellationpolicyInfo.get(i)));
		}
		
		setupDetails.setStandardInfo(stdInfoList);
		setupDetails.setInventoryInfo(inventoryList);
		setupDetails.setContractinfo(contractlist);
		setupDetails.setRoomCombinations(roomlist);
		setupDetails.setTaxInfo(taxList);
		setupDetails.setBillingInfo(billingList);
		setupDetails.setCancellationPolicyInfo(canPolicyList);
		
		return setupDetails;
	}
	
}
