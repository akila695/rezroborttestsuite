package com.rezgateway.automation.Objects;

public class HotelRoomAndInventoryInfo {

	String hotelName, roomType, bedType, ratePlan, childrenAllowed, stdAdults, adntAdults, 
	maxChildren, maxOccupancy, ratePlanCheck, inventoryTypeAllotment, inventoryTypeFreeSell, inventoryTypeOnReq, allotmentSearchSatisfiedMobile, allotmentSearchSatisfiedWeb, allotmentSearchSatisfiedCC, allotmentSearchSatisfiedWebServices,
	allotmentInventoryExhaustedMobile, allotmentInventoryExhaustedWeb, allotmentInventoryExhaustedcc, allotmentInventoryExhaustedWebServices,
	allotmentCutoffAppliedMobile, allotmentCutoffAppliedWeb, allotmentCutoffAppliedCC, allotmentCutoffAppliedWebServices,
	freeSellSearchSatisfiedMobile, freeSellSearchSatisfiedWeb, freeSellSearchSatisfiedCC, freeSellSearchSatisfiedWebServices,
	freeSellCutoffAppliedMobile, freeSellCutoffAppliedWeb, freeSellCutoffAppliedCC, freeSellCutoffAppliedWebServices,
	onRequestSearchSatisfiedMobile, onRequestSearchSatisfiedWeb, onRequestSearchSatisfiedCC, onRequestSearchSatisfiedWebServices,
	onRequestCutoffAppliedMobile, onRequestCutoffAppliedWeb, onRequestCutoffAppliedCC, onRequestCutoffAppliedWebServices, multiChildAge;

	public HotelRoomAndInventoryInfo getDetails(String[] values)
	{
		this.setHotelName								(values[0]);
		this.setRoomType								(values[1]);
		this.setBedType									(values[2]);
		this.setRatePlan								(values[3]);
		this.setChildrenAllowed							(values[4]);
		this.setStdAdults								(values[5]);
		this.setAdntAdults								(values[6]);
		this.setMaxChildren								(values[7]);
		this.setMaxOccupancy							(values[8]);
		this.setRatePlanCheck							(values[9]);
		this.setInventoryTypeAllotment					(values[10]);
		this.setInventoryTypeFreeSell					(values[11]);
		this.setInventoryTypeOnReq						(values[12]);
		this.setAllotmentInventoryExhaustedMobile		(values[13]);
		this.setAllotmentInventoryExhaustedWeb			(values[14]);
		this.setAllotmentInventoryExhaustedcc			(values[15]);
		this.setAllotmentInventoryExhaustedWebServices	(values[16]);
		this.setAllotmentSearchSatisfiedMobile			(values[17]);
		this.setAllotmentSearchSatisfiedWeb				(values[18]);
		this.setAllotmentSearchSatisfiedCC				(values[19]);
		this.setAllotmentSearchSatisfiedWebServices		(values[20]);
		this.setAllotmentCutoffAppliedMobile			(values[21]);
		this.setAllotmentCutoffAppliedWeb				(values[22]);
		this.setAllotmentCutoffAppliedCC				(values[23]);
		this.setAllotmentCutoffAppliedWebServices		(values[24]);
		this.setFreeSellSearchSatisfiedMobile			(values[25]);
		this.setFreeSellSearchSatisfiedWeb				(values[26]);
		this.setFreeSellSearchSatisfiedCC				(values[27]);
		this.setFreeSellSearchSatisfiedWebServices		(values[28]);
		this.setFreeSellCutoffAppliedMobile				(values[29]);
		this.setFreeSellCutoffAppliedWeb				(values[30]);
		this.setFreeSellCutoffAppliedCC					(values[31]);
		this.setFreeSellCutoffAppliedWebServices		(values[32]);
		this.setOnRequestSearchSatisfiedCC				(values[33]);
		this.setOnRequestSearchSatisfiedMobile			(values[34]);
		this.setOnRequestSearchSatisfiedWeb				(values[35]);
		this.setOnRequestSearchSatisfiedWebServices		(values[36]);
		this.setOnRequestCutoffAppliedCC				(values[37]);
		this.setOnRequestCutoffAppliedMobile			(values[38]);
		this.setOnRequestCutoffAppliedWeb				(values[39]);
		this.setOnRequestCutoffAppliedWebServices		(values[40]);
		this.setMultiChildAge							(values[41]);
		
		
		
		
		return this;
	}
	
	
	
	public String getMultiChildAge() {
		return multiChildAge;
	}



	public void setMultiChildAge(String multiChildAge) {
		this.multiChildAge = multiChildAge;
	}



	public String getAllotmentSearchSatisfiedMobile() {
		return allotmentSearchSatisfiedMobile;
	}



	public void setAllotmentSearchSatisfiedMobile(
			String allotmentSearchSatisfiedMobile) {
		this.allotmentSearchSatisfiedMobile = allotmentSearchSatisfiedMobile;
	}



	public String getAllotmentSearchSatisfiedWeb() {
		return allotmentSearchSatisfiedWeb;
	}



	public void setAllotmentSearchSatisfiedWeb(String allotmentSearchSatisfiedWeb) {
		this.allotmentSearchSatisfiedWeb = allotmentSearchSatisfiedWeb;
	}



	public String getAllotmentSearchSatisfiedCC() {
		return allotmentSearchSatisfiedCC;
	}



	public void setAllotmentSearchSatisfiedCC(String allotmentSearchSatisfiedCC) {
		this.allotmentSearchSatisfiedCC = allotmentSearchSatisfiedCC;
	}



	public String getAllotmentSearchSatisfiedWebServices() {
		return allotmentSearchSatisfiedWebServices;
	}



	public void setAllotmentSearchSatisfiedWebServices(
			String allotmentSearchSatisfiedWebServices) {
		this.allotmentSearchSatisfiedWebServices = allotmentSearchSatisfiedWebServices;
	}



	public String getAllotmentInventoryExhaustedMobile() {
		return allotmentInventoryExhaustedMobile;
	}



	public void setAllotmentInventoryExhaustedMobile(
			String allotmentInventoryExhaustedMobile) {
		this.allotmentInventoryExhaustedMobile = allotmentInventoryExhaustedMobile;
	}



	public String getAllotmentInventoryExhaustedWeb() {
		return allotmentInventoryExhaustedWeb;
	}



	public void setAllotmentInventoryExhaustedWeb(
			String allotmentInventoryExhaustedWeb) {
		this.allotmentInventoryExhaustedWeb = allotmentInventoryExhaustedWeb;
	}



	public String getAllotmentInventoryExhaustedcc() {
		return allotmentInventoryExhaustedcc;
	}



	public void setAllotmentInventoryExhaustedcc(
			String allotmentInventoryExhaustedcc) {
		this.allotmentInventoryExhaustedcc = allotmentInventoryExhaustedcc;
	}



	public String getAllotmentInventoryExhaustedWebServices() {
		return allotmentInventoryExhaustedWebServices;
	}



	public void setAllotmentInventoryExhaustedWebServices(
			String allotmentInventoryExhaustedWebServices) {
		this.allotmentInventoryExhaustedWebServices = allotmentInventoryExhaustedWebServices;
	}



	public String getAllotmentCutoffAppliedMobile() {
		return allotmentCutoffAppliedMobile;
	}



	public void setAllotmentCutoffAppliedMobile(String allotmentCutoffAppliedMobile) {
		this.allotmentCutoffAppliedMobile = allotmentCutoffAppliedMobile;
	}



	public String getAllotmentCutoffAppliedWeb() {
		return allotmentCutoffAppliedWeb;
	}



	public void setAllotmentCutoffAppliedWeb(String allotmentCutoffAppliedWeb) {
		this.allotmentCutoffAppliedWeb = allotmentCutoffAppliedWeb;
	}



	public String getAllotmentCutoffAppliedCC() {
		return allotmentCutoffAppliedCC;
	}



	public void setAllotmentCutoffAppliedCC(String allotmentCutoffAppliedCC) {
		this.allotmentCutoffAppliedCC = allotmentCutoffAppliedCC;
	}



	public String getAllotmentCutoffAppliedWebServices() {
		return allotmentCutoffAppliedWebServices;
	}



	public void setAllotmentCutoffAppliedWebServices(
			String allotmentCutoffAppliedWebServices) {
		this.allotmentCutoffAppliedWebServices = allotmentCutoffAppliedWebServices;
	}



	public String getFreeSellSearchSatisfiedMobile() {
		return freeSellSearchSatisfiedMobile;
	}



	public void setFreeSellSearchSatisfiedMobile(
			String freeSellSearchSatisfiedMobile) {
		this.freeSellSearchSatisfiedMobile = freeSellSearchSatisfiedMobile;
	}



	public String getFreeSellSearchSatisfiedWeb() {
		return freeSellSearchSatisfiedWeb;
	}



	public void setFreeSellSearchSatisfiedWeb(String freeSellSearchSatisfiedWeb) {
		this.freeSellSearchSatisfiedWeb = freeSellSearchSatisfiedWeb;
	}



	public String getFreeSellSearchSatisfiedCC() {
		return freeSellSearchSatisfiedCC;
	}



	public void setFreeSellSearchSatisfiedCC(String freeSellSearchSatisfiedCC) {
		this.freeSellSearchSatisfiedCC = freeSellSearchSatisfiedCC;
	}



	public String getFreeSellSearchSatisfiedWebServices() {
		return freeSellSearchSatisfiedWebServices;
	}



	public void setFreeSellSearchSatisfiedWebServices(
			String freeSellSearchSatisfiedWebServices) {
		this.freeSellSearchSatisfiedWebServices = freeSellSearchSatisfiedWebServices;
	}



	public String getFreeSellCutoffAppliedMobile() {
		return freeSellCutoffAppliedMobile;
	}



	public void setFreeSellCutoffAppliedMobile(String freeSellCutoffAppliedMobile) {
		this.freeSellCutoffAppliedMobile = freeSellCutoffAppliedMobile;
	}



	public String getFreeSellCutoffAppliedWeb() {
		return freeSellCutoffAppliedWeb;
	}



	public void setFreeSellCutoffAppliedWeb(String freeSellCutoffAppliedWeb) {
		this.freeSellCutoffAppliedWeb = freeSellCutoffAppliedWeb;
	}



	public String getFreeSellCutoffAppliedCC() {
		return freeSellCutoffAppliedCC;
	}



	public void setFreeSellCutoffAppliedCC(String freeSellCutoffAppliedCC) {
		this.freeSellCutoffAppliedCC = freeSellCutoffAppliedCC;
	}



	public String getFreeSellCutoffAppliedWebServices() {
		return freeSellCutoffAppliedWebServices;
	}



	public void setFreeSellCutoffAppliedWebServices(
			String freeSellCutoffAppliedWebServices) {
		this.freeSellCutoffAppliedWebServices = freeSellCutoffAppliedWebServices;
	}



	public String getOnRequestSearchSatisfiedMobile() {
		return onRequestSearchSatisfiedMobile;
	}



	public void setOnRequestSearchSatisfiedMobile(
			String onRequestSearchSatisfiedMobile) {
		this.onRequestSearchSatisfiedMobile = onRequestSearchSatisfiedMobile;
	}



	public String getOnRequestSearchSatisfiedWeb() {
		return onRequestSearchSatisfiedWeb;
	}



	public void setOnRequestSearchSatisfiedWeb(String onRequestSearchSatisfiedWeb) {
		this.onRequestSearchSatisfiedWeb = onRequestSearchSatisfiedWeb;
	}



	public String getOnRequestSearchSatisfiedCC() {
		return onRequestSearchSatisfiedCC;
	}



	public void setOnRequestSearchSatisfiedCC(String onRequestSearchSatisfiedCC) {
		this.onRequestSearchSatisfiedCC = onRequestSearchSatisfiedCC;
	}



	public String getOnRequestSearchSatisfiedWebServices() {
		return onRequestSearchSatisfiedWebServices;
	}



	public void setOnRequestSearchSatisfiedWebServices(
			String onRequestSearchSatisfiedWebServices) {
		this.onRequestSearchSatisfiedWebServices = onRequestSearchSatisfiedWebServices;
	}



	public String getOnRequestCutoffAppliedMobile() {
		return onRequestCutoffAppliedMobile;
	}



	public void setOnRequestCutoffAppliedMobile(String onRequestCutoffAppliedMobile) {
		this.onRequestCutoffAppliedMobile = onRequestCutoffAppliedMobile;
	}



	public String getOnRequestCutoffAppliedWeb() {
		return onRequestCutoffAppliedWeb;
	}



	public void setOnRequestCutoffAppliedWeb(String onRequestCutoffAppliedWeb) {
		this.onRequestCutoffAppliedWeb = onRequestCutoffAppliedWeb;
	}



	public String getOnRequestCutoffAppliedCC() {
		return onRequestCutoffAppliedCC;
	}



	public void setOnRequestCutoffAppliedCC(String onRequestCutoffAppliedCC) {
		this.onRequestCutoffAppliedCC = onRequestCutoffAppliedCC;
	}



	public String getOnRequestCutoffAppliedWebServices() {
		return onRequestCutoffAppliedWebServices;
	}



	public void setOnRequestCutoffAppliedWebServices(
			String onRequestCutoffAppliedWebServices) {
		this.onRequestCutoffAppliedWebServices = onRequestCutoffAppliedWebServices;
	}



	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getBedType() {
		return bedType;
	}

	public void setBedType(String bedType) {
		this.bedType = bedType;
	}

	public String getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

	public String getChildrenAllowed() {
		return childrenAllowed;
	}

	public void setChildrenAllowed(String childrenAllowed) {
		this.childrenAllowed = childrenAllowed;
	}

	public String getStdAdults() {
		return stdAdults;
	}

	public void setStdAdults(String stdAdults) {
		this.stdAdults = stdAdults;
	}

	public String getAdntAdults() {
		return adntAdults;
	}

	public void setAdntAdults(String adntAdults) {
		this.adntAdults = adntAdults;
	}

	public String getMaxChildren() {
		return maxChildren;
	}

	public void setMaxChildren(String maxChildren) {
		this.maxChildren = maxChildren;
	}

	public String getMaxOccupancy() {
		return maxOccupancy;
	}

	public void setMaxOccupancy(String maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}

	public String getRatePlanCheck() {
		return ratePlanCheck;
	}

	public void setRatePlanCheck(String ratePlanCheck) {
		this.ratePlanCheck = ratePlanCheck;
	}



	public String getInventoryTypeAllotment() {
		return inventoryTypeAllotment;
	}



	public void setInventoryTypeAllotment(String inventoryTypeAllotment) {
		this.inventoryTypeAllotment = inventoryTypeAllotment;
	}



	public String getInventoryTypeFreeSell() {
		return inventoryTypeFreeSell;
	}



	public void setInventoryTypeFreeSell(String inventoryTypeFreeSell) {
		this.inventoryTypeFreeSell = inventoryTypeFreeSell;
	}



	public String getInventoryTypeOnReq() {
		return inventoryTypeOnReq;
	}



	public void setInventoryTypeOnReq(String inventoryTypeOnReq) {
		this.inventoryTypeOnReq = inventoryTypeOnReq;
	}

	
	
}
