package com.rezgateway.automation.Objects;

public class HotelCancellationPolicyDetails {

	String hotelName, contract,  rateContract, from, to, cancellationBuffer, arrivalLessThan, stdBasedOn, stdValue, 
	noShowFeeBasedOn, noShowValue;

	public HotelCancellationPolicyDetails getDetails(String[] values)
	{
		this.setHotelName			(values[0]);
		this.setHotelContract		(values[1]);
		this.setRateContract		(values[2]);
		this.setFrom				(values[3]);
		this.setTo					(values[4]);
		this.setCancellationBuffer	(values[5]);
		this.setArrivalLessThan		(values[6]);
		this.setStdBasedOn			(values[7]);
		this.setStdValue			(values[8]);
		this.setNoShowFeeBasedOn	(values[9]);
		this.setNoShowValue			(values[10]);
		
		return this;
	}
	
	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelContract() {
		return contract;
	}

	public void setHotelContract(String hotelContract) {
		this.contract = hotelContract;
	}

	public String getRateContract() {
		return rateContract;
	}

	public void setRateContract(String rateContract) {
		this.rateContract = rateContract;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCancellationBuffer() {
		return cancellationBuffer;
	}

	public void setCancellationBuffer(String cancellationBuffer) {
		this.cancellationBuffer = cancellationBuffer;
	}

	public String getArrivalLessThan() {
		return arrivalLessThan;
	}

	public void setArrivalLessThan(String arrivalLessThan) {
		this.arrivalLessThan = arrivalLessThan;
	}

	public String getStdBasedOn() {
		return stdBasedOn;
	}

	public void setStdBasedOn(String stdBasedOn) {
		this.stdBasedOn = stdBasedOn;
	}

	public String getStdValue() {
		return stdValue;
	}

	public void setStdValue(String stdValue) {
		this.stdValue = stdValue;
	}

	public String getNoShowFeeBasedOn() {
		return noShowFeeBasedOn;
	}

	public void setNoShowFeeBasedOn(String noShowFeeBasedOn) {
		this.noShowFeeBasedOn = noShowFeeBasedOn;
	}

	public String getNoShowValue() {
		return noShowValue;
	}

	public void setNoShowValue(String noShowValue) {
		this.noShowValue = noShowValue;
	}
	
	
	
	
	
}
