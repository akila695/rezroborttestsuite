package com.rezgateway.automation.Objects;

public class TransferCancellationPolicyDetails {

	String supplier = ""; 
	String from = ""; 
	String to = ""; 
	String serviceType = ""; 
	String vehicleType = ""; 
	String cancellationBuffer = ""; 
	String pickupLessThan = ""; 
	String standardBasedOn = ""; 
	String standardValue = ""; 
	String noShowBasedOn = ""; 
	String noShowValue = "";
	
	public TransferCancellationPolicyDetails getDetails(String[] values)
	{
		this.setSupplier			(values[0]);
		this.setFrom				(values[1]);
		this.setTo					(values[2]);
		this.setServiceType			(values[3]);
		this.setVehicleType			(values[4]);
		this.setCancellationBuffer	(values[5]);
		this.setPickupLessThan		(values[6]);
		this.setStandardBasedOn		(values[7]);
		this.setStandardValue		(values[8]);
		this.setNoShowBasedOn		(values[9]);
		this.setNoShowValue			(values[10]);
		
		return this;
	}
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getCancellationBuffer() {
		return cancellationBuffer;
	}
	public void setCancellationBuffer(String cancellationBuffer) {
		this.cancellationBuffer = cancellationBuffer;
	}
	public String getPickupLessThan() {
		return pickupLessThan;
	}
	public void setPickupLessThan(String pickupLessThan) {
		this.pickupLessThan = pickupLessThan;
	}
	public String getStandardBasedOn() {
		return standardBasedOn;
	}
	public void setStandardBasedOn(String standardBasedOn) {
		this.standardBasedOn = standardBasedOn;
	}
	public String getStandardValue() {
		return standardValue;
	}
	public void setStandardValue(String standardValue) {
		this.standardValue = standardValue;
	}
	public String getNoShowBasedOn() {
		return noShowBasedOn;
	}
	public void setNoShowBasedOn(String noShowBasedOn) {
		this.noShowBasedOn = noShowBasedOn;
	}
	public String getNoShowValue() {
		return noShowValue;
	}
	public void setNoShowValue(String noShowValue) {
		this.noShowValue = noShowValue;
	}
	
	
}
