package com.rezgateway.automation.Objects;

public class TransferCityZoneConfiguration {

	String supplier = ""; 
	String country = ""; 
	String city = ""; 
	String zoneName = "";
	
	public TransferCityZoneConfiguration getDetails(String[] values)
	{
		this.setSupplier	(values[0]);
		this.setCountry		(values[1]);
		this.setCity		(values[2]);
		this.setZoneName	(values[3]);
		
		return this;
	}
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
	
}
