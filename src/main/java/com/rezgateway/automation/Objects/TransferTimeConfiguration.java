package com.rezgateway.automation.Objects;

public class TransferTimeConfiguration {

	String supplier = ""; 
	String privateStartHrs = ""; 
	String privateStartMins = ""; 
	String privateEndHrs = ""; 
	String privateEndMins = ""; 
	String sharedStartHrs = ""; 
	String sharedStartMins = ""; 
	String sharedEndHrs = ""; 
	String sharedEndMins = ""; 
	String freequencyHrs = ""; 
	String freequencyMins = "";
	
	public TransferTimeConfiguration getDetails(String[] values)
	{
		this.setSupplier			(values[0]);
		this.setPrivateStartHrs		(values[1]);
		this.setPrivateStartMins	(values[2]);
		this.setPrivateEndHrs		(values[3]);
		this.setPrivateEndMins		(values[4]);
		this.setSharedStartHrs		(values[5]);
		this.setSharedStartMins		(values[6]);
		this.setSharedEndHrs		(values[7]);
		this.setSharedEndMins		(values[8]);
		this.setFreequencyHrs		(values[9]);
		this.setFreequencyMins		(values[10]);
		
		return this;
	}
	
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getPrivateStartHrs() {
		return privateStartHrs;
	}
	public void setPrivateStartHrs(String privateStartHrs) {
		this.privateStartHrs = privateStartHrs;
	}
	public String getPrivateStartMins() {
		return privateStartMins;
	}
	public void setPrivateStartMins(String privateStartMins) {
		this.privateStartMins = privateStartMins;
	}
	public String getPrivateEndHrs() {
		return privateEndHrs;
	}
	public void setPrivateEndHrs(String privateEndHrs) {
		this.privateEndHrs = privateEndHrs;
	}
	public String getPrivateEndMins() {
		return privateEndMins;
	}
	public void setPrivateEndMins(String privateEndMins) {
		this.privateEndMins = privateEndMins;
	}
	public String getSharedStartHrs() {
		return sharedStartHrs;
	}
	public void setSharedStartHrs(String sharedStartHrs) {
		this.sharedStartHrs = sharedStartHrs;
	}
	public String getSharedStartMins() {
		return sharedStartMins;
	}
	public void setSharedStartMins(String sharedStartMins) {
		this.sharedStartMins = sharedStartMins;
	}
	public String getSharedEndHrs() {
		return sharedEndHrs;
	}
	public void setSharedEndHrs(String sharedEndHrs) {
		this.sharedEndHrs = sharedEndHrs;
	}
	public String getSharedEndMins() {
		return sharedEndMins;
	}
	public void setSharedEndMins(String sharedEndMins) {
		this.sharedEndMins = sharedEndMins;
	}
	public String getFreequencyHrs() {
		return freequencyHrs;
	}
	public void setFreequencyHrs(String freequencyHrs) {
		this.freequencyHrs = freequencyHrs;
	}
	public String getFreequencyMins() {
		return freequencyMins;
	}
	public void setFreequencyMins(String freequencyMins) {
		this.freequencyMins = freequencyMins;
	}
	
	
}
