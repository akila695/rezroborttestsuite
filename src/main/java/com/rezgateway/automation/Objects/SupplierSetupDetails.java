package com.rezgateway.automation.Objects;

public class SupplierSetupDetails {

	String action = ""; 
	String supplierCode = ""; 
	String supplierName = ""; 
	String productType = ""; 
	String address1 = ""; 
	String country = ""; 
	String city = ""; 
	String currency = ""; 
	String contactname = ""; 
	String email = ""; 
	String contactType = ""; 
	String contractFrom = ""; 
	String contractTo = "";
	
	public SupplierSetupDetails getDetails(String[] values)
	{
		this.setAction			(values[0]);
		this.setSupplierCode	(values[1]);
		this.setSupplierName	(values[2]);
		this.setProductType		(values[3]);
		this.setAddress1		(values[4]);
		this.setCountry			(values[5]);
		this.setCity			(values[6]);
		this.setCurrency		(values[7]);
		this.setContactname		(values[8]);
		this.setEmail			(values[9]);
		this.setContactType		(values[10]);
		this.setContractFrom	(values[11]);
		this.setContractTo		(values[12]);
		
		return this;
	}
	
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getContactname() {
		return contactname;
	}
	public void setContactname(String contactname) {
		this.contactname = contactname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactType() {
		return contactType;
	}
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	public String getContractFrom() {
		return contractFrom;
	}
	public void setContractFrom(String contractFrom) {
		this.contractFrom = contractFrom;
	}
	public String getContractTo() {
		return contractTo;
	}
	public void setContractTo(String contractTo) {
		this.contractTo = contractTo;
	}
	
	
}
