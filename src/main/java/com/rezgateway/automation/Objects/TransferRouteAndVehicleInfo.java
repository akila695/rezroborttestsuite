package com.rezgateway.automation.Objects;

public class TransferRouteAndVehicleInfo {

	String supplier = ""; 
	String route = ""; 
	String privateVehicleAvailability = "";
	String privateVehicle = ""; 
	String sharedVehicleAvailability = "";
	String sharedVehicle = "";
	
	public TransferRouteAndVehicleInfo getDetails(String[] values)
	{
		this.setSupplier					(values[0]);
		this.setRoute						(values[1]);
		this.setPrivateVehicleAvailability	(values[2]);
		this.setPrivateVehicle				(values[3]);
		this.setSharedVehicleAvailability	(values[4]);
		this.setSharedVehicle				(values[5]);
		
		return this;
	}
	
	
	public String getPrivateVehicleAvailability() {
		return privateVehicleAvailability;
	}


	public void setPrivateVehicleAvailability(String privateVehicleAvailability) {
		this.privateVehicleAvailability = privateVehicleAvailability;
	}


	public String getSharedVehicleAvailability() {
		return sharedVehicleAvailability;
	}


	public void setSharedVehicleAvailability(String sharedVehicleAvailability) {
		this.sharedVehicleAvailability = sharedVehicleAvailability;
	}


	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getPrivateVehicle() {
		return privateVehicle;
	}
	public void setPrivateVehicle(String privateVehicle) {
		this.privateVehicle = privateVehicle;
	}
	public String getSharedVehicle() {
		return sharedVehicle;
	}
	public void setSharedVehicle(String sharedVehicle) {
		this.sharedVehicle = sharedVehicle;
	} 
	
	
}
