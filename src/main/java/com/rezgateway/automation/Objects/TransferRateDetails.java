package com.rezgateway.automation.Objects;

public class TransferRateDetails {

	String supplierName = ""; 
	String ratePeriodFrom = ""; 
	String ratePeriodTo = ""; 
	String route = ""; 
	String vehicleType = ""; 
	String privateOrShared = ""; 
	String dateType = ""; 
	String periodFrom = ""; 
	String periodTo = ""; 
	String daysOfTheWeek = ""; 
	String rates = ""; 
	String blackout = ""; 
	String cutoff = ""; 
	String atobOneway = ""; 
	String atobRound = ""; 
	String btoaOneway = ""; 
	String btoaRound = ""; 
	String cutOffDays = ""; 
	String blackoutYesOrNo = ""; 
	String blackoutReason = "";
	
	public TransferRateDetails getDetails(String[] values)
	{
		this.setSupplierName			(values[0]);
		this.setRatePeriodFrom			(values[1]);
		this.setRatePeriodTo			(values[2]);
		this.setRoute					(values[3]);
		this.setVehicleType				(values[4]);
		this.setPrivateOrShared			(values[5]);
		this.setDateType				(values[6]);
		this.setPeriodFrom				(values[7]);
		this.setPeriodTo				(values[8]);
		this.setDaysOfTheWeek			(values[9]);
		this.setRates					(values[10]);
		this.setBlackout				(values[11]);
		this.setCutoff					(values[12]);
		this.setAtobOneway				(values[13]);
		this.setAtobRound				(values[14]);
		this.setBtoaOneway				(values[15]);
		this.setBtoaRound				(values[16]);
		this.setCuffOffDays				(values[17]);
		this.setBlackoutYesOrNo			(values[18]);
		this.setBlackoutReason			(values[19]);
		
		return this;
	}
	
	
	
	
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getRatePeriodFrom() {
		return ratePeriodFrom;
	}
	public void setRatePeriodFrom(String ratePeriodFrom) {
		this.ratePeriodFrom = ratePeriodFrom;
	}
	public String getRatePeriodTo() {
		return ratePeriodTo;
	}
	public void setRatePeriodTo(String ratePeriodTo) {
		this.ratePeriodTo = ratePeriodTo;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getPrivateOrShared() {
		return privateOrShared;
	}
	public void setPrivateOrShared(String privateOrShared) {
		this.privateOrShared = privateOrShared;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getPeriodFrom() {
		return periodFrom;
	}
	public void setPeriodFrom(String periodFrom) {
		this.periodFrom = periodFrom;
	}
	public String getPeriodTo() {
		return periodTo;
	}
	public void setPeriodTo(String periodTo) {
		this.periodTo = periodTo;
	}
	public String getDaysOfTheWeek() {
		return daysOfTheWeek;
	}
	public void setDaysOfTheWeek(String daysOfTheWeek) {
		this.daysOfTheWeek = daysOfTheWeek;
	}
	public String getRates() {
		return rates;
	}
	public void setRates(String rates) {
		this.rates = rates;
	}
	public String getBlackout() {
		return blackout;
	}
	public void setBlackout(String blackout) {
		this.blackout = blackout;
	}
	public String getCutoff() {
		return cutoff;
	}
	public void setCutoff(String cutoff) {
		this.cutoff = cutoff;
	}
	public String getAtobOneway() {
		return atobOneway;
	}
	public void setAtobOneway(String atobOneway) {
		this.atobOneway = atobOneway;
	}
	public String getAtobRound() {
		return atobRound;
	}
	public void setAtobRound(String atobRound) {
		this.atobRound = atobRound;
	}
	public String getBtoaOneway() {
		return btoaOneway;
	}
	public void setBtoaOneway(String btoaOneway) {
		this.btoaOneway = btoaOneway;
	}
	public String getBtoaRound() {
		return btoaRound;
	}
	public void setBtoaRound(String btoaRound) {
		this.btoaRound = btoaRound;
	}
	public String getCuffOffDays() {
		return cutOffDays;
	}
	public void setCuffOffDays(String cuffOffDays) {
		this.cutOffDays = cuffOffDays;
	}
	public String getBlackoutYesOrNo() {
		return blackoutYesOrNo;
	}
	public void setBlackoutYesOrNo(String blackoutYesOrNo) {
		this.blackoutYesOrNo = blackoutYesOrNo;
	}
	public String getBlackoutReason() {
		return blackoutReason;
	}
	public void setBlackoutReason(String blackoutReason) {
		this.blackoutReason = blackoutReason;
	}
	
	
}
