package com.rezgateway.automation.Objects;

public class TransferContractInfo {

	String supplier = "";
	String from = "";
	String to = ""; 
	String contractType = "";
	String comType = "";
	
	
	public TransferContractInfo getDetails(String[] values)
	{
		this.setSupplier		(values[0]);
		this.setFrom			(values[1]);
		this.setTo				(values[2]);
		this.setContractType	(values[3]);
		this.setComType			(values[4]);
		
		return this;
	}
	
	
	public String getComType() {
		return comType;
	}

	public void setComType(String comType) {
		this.comType = comType;
	}


	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	
	
	
	
}
