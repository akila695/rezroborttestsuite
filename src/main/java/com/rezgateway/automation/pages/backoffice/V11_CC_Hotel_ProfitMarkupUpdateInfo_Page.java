package com.rezgateway.automation.pages.backoffice;

import org.openqa.selenium.WebElement;

import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_ProfitMarkupUpdateInfo_Page extends PageObject {

	// Checking the entered date period is added to Margin update grid
	public void recoredIsAvailable() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("element_markupList_xpath").checkElementVisisbilityWithException(1, "Date record is not adding to the MarginUpdate grid");
		switchToDefaultFrame();
	}

	public WebElement availablePeriod() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		WebElement ele = getElement("label_period_xpath").getWebElement();
		switchToDefaultFrame();
		return ele;
	}

	public WebElement availableDays() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		WebElement ele = getElement("label_selectedDays_xpath").getWebElement();
		switchToDefaultFrame();
		return ele;
	}

	public void selectMarginType(String marginType) throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		switch (marginType) {
		case "PEC":
			getElement("radio_marginTypePercentage_xpath").click();
			break;
		case "FIX":
			getElement("radio_marginTypeFixedPacakge_xpath").click();
			break;
		default:
			getElement("radio_marginTypePercentage_xpath").click();
			break;
		}
		switchToDefaultFrame();
	}
	
	//Set the Standard  hotel mark-up value
	public void setHotelMarkUp(String markupValue) throws Exception{
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("inputfeild_margin_xpath").setText(markupValue);
		switchToDefaultFrame();
	}
	
	//Set the Additional Adult profit mark-up value
	public void selectAdditionalAdultMarkup(String markupValue) throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("inputfeild_AdditionalAdultmargin_xpath").setText(markupValue);
		switchToDefaultFrame();
	}
	
	//Set the Child profit mark-up value
	public void selectChildMarkup(String markupValue) throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("inputfeild_Childmargin_xpath").setText(markupValue);
		switchToDefaultFrame();
	}
	
	public void selectTheViewMarkup(){
		// ${TODO}: implement the checking added record.
	}
	
	//remove the added record.
	public void removeTheRecord() throws Exception{
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("button_deleteRecored_xpath").click();
		switchToDefaultFrame();
	}
	
	//Click the Save button and Return the Save Message Element
	public WebElement saveTheDetails() throws Exception{
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("button_save_xpath").click();
		WebElement ele = getElement("alert_alertMessage_id").getWebElement();
		switchToDefaultFrame();
		return ele;
	}
	
	//for clear the All data
	public void clearTheAllData() throws Exception{
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("button_clear_xpath").click();
		switchToDefaultFrame();
	}
	
}
