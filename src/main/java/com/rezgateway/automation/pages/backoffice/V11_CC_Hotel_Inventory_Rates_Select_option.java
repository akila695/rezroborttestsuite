package com.rezgateway.automation.pages.backoffice;

import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_Inventory_Rates_Select_option extends PageObject {

	public void selectInventoryAndRates() throws Exception {

		getElementClick("RadioButton_Inventory_Rates_Xpath");

	}

	public void selectInventory() throws Exception {

		getElementClick("RadioButton_Inventory_Xpath");

	}

	public void selectRates() throws Exception {

		getElementClick("RadioButton_Rates_Xpath");

	}

}
