package com.rezgateway.automation.pages.backoffice;

import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page extends PageObject{

	public void setDatePeriod(HotelContractInfo contractInfo) throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		executejavascript("document.getElementsById('datepicker1')[0].value='" + contractInfo.getPeriodFrom() + "';");
		executejavascript("document.getElementsById('datepicker2')[0].value='" + contractInfo.getPeriodTo() + "';");
		switchToDefaultFrame();
		// ${TODO}: Implement the Selecting "Select Day(s)". 
		
	}

	//Add the Entered dates to the Mark-up Update page 
	public V11_CC_Hotel_ProfitMarkupUpdateInfo_Page addingDatePeriodToUpdateInfoPage() throws Exception{
		V11_CC_Hotel_ProfitMarkupUpdateInfo_Page updateInfoPage = new V11_CC_Hotel_ProfitMarkupUpdateInfo_Page();
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("button_add_xpath").click();
		switchToDefaultFrame();
		return updateInfoPage;
	}
	
	//view the mark-up Details.
	public V11_CC_Hotel_ProfitMarkupView_Page proccedToProfitMarkupViewPage() throws Exception{
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("button_view_xpath").click();
		switchToDefaultFrame();
		V11_CC_Hotel_ProfitMarkupView_Page profitMarkupViewPage = new V11_CC_Hotel_ProfitMarkupView_Page();
		return profitMarkupViewPage;
	}
	
}


