package com.rezgateway.automation.pages.backoffice;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.utill.*;
import com.rezgateway.automation.dataObject.V11_Hotel_Contracts_Object;

public class V11_CC_Select_Room_Types_Page {

	public void Select_Room_Types(com.rezgateway.automation.Objects.HotelRoomCombinations roomdetails, com.rezgateway.automation.Objects.HotelContractInfo cont) throws Exception {

		// WebElement outerelement = driver.findElement(By.className("select-contract"));
		// System.out.println(outerelement.getText());
		if (roomdetails.getContractName().equalsIgnoreCase(cont.getContractname())) {

			Thread.sleep(10000);

			DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[3]/div/div/div[2]/div[1]/div[1]/label/input")).click();

			Thread.sleep(5000);

			ArrayList<WebElement> elements = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("irate-control-label")));

			for (WebElement eli : elements) {

				if (eli.getText().trim().equalsIgnoreCase(roomdetails.getRoomType())) {

					eli.click();

				}
			}

		}
	}

	public void SelectInventory_Options(V11_Hotel_Contracts_Object con) throws Exception {

		Thread.sleep(10000);

		ArrayList<WebElement> elements = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("irate-control-label")));

		for (WebElement eli : elements) {

			for (int i = 0; i < con.getSelectInventoryOptions().length; i++) {

				if (eli.getText().trim().equalsIgnoreCase(con.getSelectInventoryOptions()[i])) {

					eli.click();
					eli.click();
				}
			}
		}

	}

}
