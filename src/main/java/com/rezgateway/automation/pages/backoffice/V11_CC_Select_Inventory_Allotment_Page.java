package com.rezgateway.automation.pages.backoffice;

import java.util.ArrayList;
import org.openqa.selenium.By;
import com.rezgateway.automation.utill.*;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelRoomCombinations;

public class V11_CC_Select_Inventory_Allotment_Page extends PageObject {

	public void selectInventoryAllotmentTypes(HotelRoomCombinations roomdetails, HotelContractInfo cont) {

		if (roomdetails.getContractName().equalsIgnoreCase(cont.getContractname())) {

			for (int i = 1; i <= 5; i++) {

				if (roomdetails.getSelect_allotment().equalsIgnoreCase(DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[2]/div/div/div[2]/div/label[" + i + "]")).getText())) {

					DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[2]/div/div/div[2]/div/label[" + i + "]")).click();
					break;

				}

			}

		}

	}

	public ArrayList<String> getAvailableInventoryAllotmentTypes(HotelRoomCombinations roomdetails, HotelContractInfo cont){
		ArrayList<String> InventoryAllotmentTypes = new ArrayList<String>();
		
		if (roomdetails.getContractName().equalsIgnoreCase(cont.getContractname())) {
			for (int i = 1; i <= 5; i++) {
				InventoryAllotmentTypes.add(DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[2]/div/div/div[2]/div/label[" + i + "]")).getText());
			}
			return InventoryAllotmentTypes;
		}else{
			return null;
		}
		
	}
}
