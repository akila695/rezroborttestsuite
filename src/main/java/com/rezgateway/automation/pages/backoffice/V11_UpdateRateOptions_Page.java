package com.rezgateway.automation.pages.backoffice;

import java.text.ParseException;
import java.util.ArrayList;

import com.rezgateway.automation.utill.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelRoomCombinations;


public class V11_UpdateRateOptions_Page extends PageObject {

	public void selectRateoptions(com.rezgateway.automation.dataObject.V11_Hotel_Contracts_Object con) throws InterruptedException {

		Thread.sleep(10000);

		DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[5]/div/div/div[2]/label[1]/input")).click();

		Thread.sleep(3000);

		ArrayList<WebElement> elements = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("irate-control-label")));

		for (WebElement eli : elements) {

			for (int i = 0; i < con.getRateOptions().length; i++) {

				if (eli.getText().trim().equalsIgnoreCase(con.getRateOptions()[i])) {

					eli.click();

				}

			}
		}
	}

	public void selectRatePeriod(com.rezgateway.automation.Objects.HotelContractInfo cont) throws ParseException, InterruptedException {

		DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='root']/div/div[1]/div/div/div[6]/div/div/div/div[1]/a[2]")).click();

		Thread.sleep(5000);

		ArrayList<WebElement> elements = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("irate-control-label")));

		for (WebElement ele : elements) {

			String fromdate = Calender.getDate("dd-MMMM-yyyy", "dd-MMM-yyyy", cont.getPeriodFrom().replaceAll("/", "-"));
			String todate = Calender.getDate("dd-MMMM-yyyy", "dd-MMM-yyyy", cont.getPeriodTo().replaceAll("/", "-"));

			if (ele.getText().equalsIgnoreCase(fromdate + " " + "-" + " " + todate + " " + "( " + dayssetter(cont) + " )")) {

				ele.click();

			}

		}

	}

	public String dayssetter(HotelContractInfo cont) {

		String Days = "";

		String week[] = cont.getDaysOfTheWeek().split("-");

		for (int i = 0; i < week.length; i++) {

			if (i == 0)
				Days = Days.concat(week[0]);
			else
				Days = Days.concat("," + week[i]);

		}

		return Days;

	}

	public void enterInventoriesandRates(HotelRoomCombinations roomdetails, HotelContractInfo cont) throws InterruptedException {

		ArrayList<WebElement> elements = new ArrayList<>(DriverFactory.getInstance().getDriver().findElements(By.className("irate-according-colapse")));

		for (WebElement ele : elements) {

			System.out.println(ele.getText());
			if (roomdetails.getContractName().equalsIgnoreCase(cont.getContractname())) {

				if (ele.getText().equalsIgnoreCase(roomdetails.getRoomType())) {

					ele.click();
					ele.click();

					Thread.sleep(5000);

					if (roomdetails.getInventory().equalsIgnoreCase("null")) {
						DriverFactory.getInstance().getDriver().findElement(By.name("roomtypes-accordion.rooms." + roomdetails.getRoomType() + ".insert-values.cutoff-amount")).sendKeys(roomdetails.getCutoff());

						DriverFactory.getInstance().getDriver().findElement(By.name("roomtypes-accordion.rooms." + roomdetails.getRoomType() + ".rate-insert." + roomdetails.getRatePlan() + "." + roomdetails.getBedType() + ".Net Rate"))
								.sendKeys(roomdetails.getNetrate());

					} else {
						DriverFactory.getInstance().getDriver().findElement(By.name("roomtypes-accordion.rooms." + roomdetails.getRoomType() + ".insert-values.allotment-amount")).sendKeys(roomdetails.getInventory());

						DriverFactory.getInstance().getDriver().findElement(By.name("roomtypes-accordion.rooms." + roomdetails.getRoomType() + ".insert-values.cutoff-amount")).sendKeys(roomdetails.getCutoff());

						DriverFactory.getInstance().getDriver().findElement(By.name("roomtypes-accordion.rooms." + roomdetails.getRoomType() + ".rate-insert." + roomdetails.getRatePlan() + "." + roomdetails.getBedType() + ".Net Rate"))
								.sendKeys(roomdetails.getNetrate());

					}

					Thread.sleep(5000);

					DriverFactory.getInstance().getDriver().findElement(By.id("save")).click();
				}
			}

			break;

		}

	}

	public void enterRates() {

	}

}
