package com.rezgateway.automation.pages.backoffice;

import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_ProfitMarkupConfiguration_Page extends PageObject {

	
	public void selectAfiliatePartnerType() {
		// ${TODO}:implement the selectAfiliatePartnerType
	}

	
	public void selectB2BPartnerType() {
		// ${TODO}:implement the selectB2BPartnerType
	}

	public void selectB2CPartnerType() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_partnerTypeB2C_xpath").click();
		switchToDefaultFrame();
	}

	
	public void selectApplicableRegions() {
		// ${TODO}:implement the selectApplicableRegions and Want to fill applicable region and Counties elements XMLs
	}

	
	public void selectApplicableCountry() {
		// ${TODO}:implement the selectApplicableCountry Want to fill applicable region and Counties elements XMLs
	}
	
	public void selectingBookingType(String bookingType) throws Exception {
				
		switch (bookingType) {
		case "All":
			selectBookingTypeAll();
			break;
		case "ShoppingCart":
			selectBookingTypeShoppingCart();
			break;
		case "DynamicPackage":
			selectBookingTypeDynamicPackage();
			break;
		case "FixedPackage":
			selectBookingTypeFixedPackage();
			break;
		default:
			selectBookingTypeAll();
			break;
		}
		
	}

	public void selectingBookingChannel(String bookingChannel) throws Exception {

		switch (bookingChannel) {
		case "All":
			selectBookingChannelAll();
			break;
		case "Mobile":
			selectBookingChannelMobile();
			break;
		case "Web":
			selectBookingChannelWeb();
			break;
		case "CC":
			selectBookingChannelCC();
			break;
		case "WebService":
			selectBookingChannelWebService();
			break;
		default:
			selectBookingTypeAll();
			break;
		}
	
	}

	// for Select the Overwrite Specific Margin status
	public void selectOverwriteSpecificMarginOption(String flag) throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		if (flag.equalsIgnoreCase("Yes")) {
			getElement("radio_OverwriteSpecificYes_xpath").click();
		} else {
			getElement("radio_OverwriteSpecificNo_xpath").click();
		}
		switchToDefaultFrame();
	}

	//Proceed To Markup Select By Dates Page
	public V11_CC_Hotel_ProfitMarkupSelectByDates_Page proccedToMarkupSelectByDatesPage() throws Exception {
		
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_selectByDate_xpath").click();
		V11_CC_Hotel_ProfitMarkupSelectByDates_Page selecBydate = new V11_CC_Hotel_ProfitMarkupSelectByDates_Page();
		switchToDefaultFrame();
		return selecBydate;
	
	}

	//Proceed To Markup Select By Period Page
	public V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page proccedToMarkupSelectByPeriodPage() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_selectByPeriod_xpath").click();
		V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page selecByPeriod = new V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page();
		switchToDefaultFrame();
		return selecByPeriod;
	}

	/******************************************************************************************************************************************/
	
	public void selectBookingTypeAll() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingTypeAll_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingTypeShoppingCart() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingTypeShoppingCart_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingTypeDynamicPackage() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingTypeDynamicPackage_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingTypeFixedPackage() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingTypeFixedPackage_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingChannelAll() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingChannelAll_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingChannelMobile() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingChannelMobile_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingChannelWeb() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingChannelWeb_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingChannelCC() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingChannelCC_xpath").click();
		switchToDefaultFrame();
	}

	public void selectBookingChannelWebService() throws Exception {
		switchToFrame("iframe_HotelProfitmarkup_id");
		getElement("radio_bookingChannelWebServices_xpath").click();
		switchToDefaultFrame();
	}

}
