package com.rezgateway.automation.pages.backoffice;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.rezgateway.automation.Objects.MarginViewGrid;
import com.rezgateway.automation.core.PageObject;

public class V11_CC_Hotel_ProfitMarkupView_Page extends PageObject {

	private String 							fromDate 		= null; 
	private String 							toDate			= null;
	private ArrayList<String> 				dates   		= null;
	private MarginViewGrid					viewGrid		= null;
	
	
			
	/**
	 * @return the period
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @return the dates
	 */
	public ArrayList<String> getDates() {
		return dates;
	}
	/**
	 * @return the viewGrid
	 */
	public MarginViewGrid getViewGrid() {
		return viewGrid;
	}
	/**
	 * @param period the period to set
	 * @throws Exception 
	 */
	public void setPeriod(String fromDate) throws Exception {
		fromDate = getElement("label_periodFromDateIntheGrid_xpath").getText();
		this.fromDate = fromDate;
	}
	/**
	 * @param dates the dates to set
	 * @throws Exception 
	 */
	public void setDates(ArrayList<String> dates) throws Exception {
		String [] datesArray = getElement("label_selectedDates_xpath").getText().split(",");
		for(int i=0 ; i<datesArray.length ; i++){
			dates.add(datesArray[i].trim());
		}
		this.dates = dates;
	}
	/**
	 * @param viewGrid the viewGrid to set
	 * @throws Exception 
	 */
	public void setViewGrid(MarginViewGrid viewGrid) throws Exception {
		
		WebElement tabelEle =  getElement("table_hotelProfitmarkup_classname").getWebElement();
		List<WebElement> tableRowEleList = tabelEle.findElements(By.tagName("tr")); 
		if(tableRowEleList.size()==1){
			viewGrid=null;
		}else{
			// ${TODO}: implement set data into MarginViewGrid object after fix that not Display old record issue.
		}
		
		this.viewGrid = viewGrid;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 * @throws Exception 
	 */
	public void setToDate(String toDate) throws Exception {
		toDate = getElement("label_periodToDateIntheGrid_xpath").getText();
		this.toDate = toDate;
	}
	
	
	
	
}
