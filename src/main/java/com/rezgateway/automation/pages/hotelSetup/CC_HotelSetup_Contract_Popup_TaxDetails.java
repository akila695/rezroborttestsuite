package com.rezgateway.automation.pages.hotelSetup;

import java.util.ArrayList;
import org.openqa.selenium.Keys;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelSetupDetails;
import com.rezgateway.automation.Objects.HotelTaxDetails;

public class CC_HotelSetup_Contract_Popup_TaxDetails extends PageObject {

	public boolean checkAvailability() {
		try {
			return getElementVisibility("inputfield_contract_name_classname", 60);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public void enterDetails(HotelSetupDetails setupDetails, String hotelName) throws Exception {

		ArrayList<HotelTaxDetails> taxList = new ArrayList<>();
		for (int i = 0; i < setupDetails.getTaxInfo().size(); i++) {
			if (hotelName.equals(setupDetails.getTaxInfo().get(i).getHotelName())) {
				taxList.add(setupDetails.getTaxInfo().get(i));
			}
		}

		for (int i = 0; i < taxList.size(); i++) {
			getElement("inputfield_contract_name_classname").setText(taxList.get(i).getContractName());
			Thread.sleep(3000);
			getElement("inputfield_contract_name_classname").setText(Keys.ARROW_DOWN);
			getElement("inputfield_contract_name_classname").setText(Keys.TAB);
			Thread.sleep(2000);

			if (taxList.get(i).getTaxAndOtherCharges().equalsIgnoreCase("not included in rate"))
				getElements("radiobutton_tax_and_other_charges_classname").get(0).click();
			else
				getElements("radiobutton_tax_and_other_charges_classname").get(1).click();

			getElement("inputfield_from_date_name").setText(taxList.get(i).getFrom());
			getElement("inputfield_to_date_name").setText(taxList.get(i).getTo());

			// sales tax
			if (taxList.get(i).getSalesTaxType().equals("percentage"))
				getElement("radiobutton_sales_tax_percentage__xpath").click();
			else
				getElement("radiobutton_sales_tax_value_xpath").click();

			getElement("inputfield_sales_tax_value_xpath").setText(taxList.get(i).getSalesTaxValue());

			// occupancy tax
			if (taxList.get(i).getOccupancyTaxType().equals("percentage"))
				getElement("radiobutton_occupancy_tax_percentage_xpath").click();
			else
				getElement("radiobutton_occupancy_tax_value_xpath").click();

			getElement("inputfield_occupancy_tax_value_xpath").setText(taxList.get(i).getOccupancyTaxValue());

			// energy tax
			if (taxList.get(i).getEnrgyTaxType().equals("percentage"))
				getElement("radiobutton_energy_tax_percentage_xpath").click();
			else
				getElement("radiobutton_enrgy_tax_value_xpath").click();

			getElement("inputfield_enrgy_tax_value_xpath").setText(taxList.get(i).getEnrgyTaxValue());

			// maid service tax
			if (taxList.get(i).getMaidServiceTaxType().equals("percentage"))
				getElement("radiobutton_maid_service_tax_percentage_xpath").click();
			else
				getElement("radiobutton_maid_service_tax_value_xpath").click();

			getElement("inputfield_maid_service_tax_value_xpath").setText(taxList.get(i).getMaidServiceTax());

			getElement("inputfield_resort_fee_xpath").setText(taxList.get(i).getResortFee());
			getElement("inputfield_miscellaneous_fee_xpath").setText(taxList.get(i).getMiscallaneousFee());

			getElement("button_add_classname").click();
		}

		getElement("button_save_classname").click();
	}

	public boolean enterDetailsInScreen(HotelSetupDetails setupDetails, String hotelName) throws Exception {
		boolean flag = false ;
		
		ArrayList<HotelTaxDetails> taxList = new ArrayList<>();
		for (int i = 0; i < setupDetails.getTaxInfo().size(); i++) {
			if (hotelName.equals(setupDetails.getTaxInfo().get(i).getHotelName())) {
				taxList.add(setupDetails.getTaxInfo().get(i));
			}
		}

		for (int i = 0; i < taxList.size(); i++) {
			getElement("inputfield_contract_name_classname").setText(taxList.get(i).getContractName());
			Thread.sleep(3000);
			getElement("inputfield_contract_name_classname").setText(Keys.ARROW_DOWN);
			getElement("inputfield_contract_name_classname").setText(Keys.TAB);
			Thread.sleep(2000);

			if (taxList.get(i).getTaxAndOtherCharges().equalsIgnoreCase("not included in rate"))
				getElements("radiobutton_tax_and_other_charges_classname").get(0).click();
			else
				getElements("radiobutton_tax_and_other_charges_classname").get(1).click();

			getElement("inputfield_from_date_name").setText(taxList.get(i).getFrom());
			getElement("inputfield_to_date_name").setText(taxList.get(i).getTo());

			// sales tax
			if (taxList.get(i).getSalesTaxType().equals("percentage"))
				getElement("radiobutton_sales_tax_percentage__xpath").click();
			else
				getElement("radiobutton_sales_tax_value_xpath").click();

			getElement("inputfield_sales_tax_value_xpath").setText(taxList.get(i).getSalesTaxValue());

			// occupancy tax
			if (taxList.get(i).getOccupancyTaxType().equals("percentage"))
				getElement("radiobutton_occupancy_tax_percentage_xpath").click();
			else
				getElement("radiobutton_occupancy_tax_value_xpath").click();

			getElement("inputfield_occupancy_tax_value_xpath").setText(taxList.get(i).getOccupancyTaxValue());

			// energy tax
			if (taxList.get(i).getEnrgyTaxType().equals("percentage"))
				getElement("radiobutton_energy_tax_percentage_xpath").click();
			else
				getElement("radiobutton_enrgy_tax_value_xpath").click();

			getElement("inputfield_enrgy_tax_value_xpath").setText(taxList.get(i).getEnrgyTaxValue());

			// maid service tax
			if (taxList.get(i).getMaidServiceTaxType().equals("percentage"))
				getElement("radiobutton_maid_service_tax_percentage_xpath").click();
			else
				getElement("radiobutton_maid_service_tax_value_xpath").click();

			getElement("inputfield_maid_service_tax_value_xpath").setText(taxList.get(i).getMaidServiceTax());

			getElement("inputfield_resort_fee_xpath").setText(taxList.get(i).getResortFee());
			getElement("inputfield_miscellaneous_fee_xpath").setText(taxList.get(i).getMiscallaneousFee());

			getElement("button_add_classname").click();
		}

		if (getElementIsClickable("button_save_classname", 3)) {
			getElement("button_save_classname").click();
			if (getelementPresence(getElement("label_SaveAlert_xpath"))) {
				flag = true;
			} else {
				flag = false;
			}
		} else {

			flag = false;
		}
		return flag;
	}

	public CC_HotelSetup_Contract closeTheHotelContractConfigScreen() throws Exception {
		
		CC_HotelSetup_Contract switchToContractPage = new CC_HotelSetup_Contract();
		
		if(getElementVisibility("button_HotelContractConfigScreenClose_xpath")){
			getElement("button_HotelContractConfigScreenClose_xpath").click();
			return switchToContractPage;
		}else{
			return null;
		}
		
		
		
	}


}
