package com.rezgateway.automation.pages.hotelSetup;

import static org.testng.Assert.assertEquals;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.internal.IResultListener;

import com.rezgateway.automation.pojo.ComboBox;
import com.rezgateway.automation.utill.DriverFactory;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelStandardInfo;

@SuppressWarnings("unused")
public class CC_HotelSetup_Standard extends PageObject {

	static Logger logger;

	public CC_HotelSetup_Standard() {
		logger = Logger.getLogger(CC_HotelSetup_Standard.class);
	}

	public boolean checkAvailability() throws Exception {
		switchToFrame("frame_standard_info_id");
		if (getElementVisibility("inputfield_hotel_name_xpath", 15))
			return true;
		else
			return false;
	}

	
	
	
	
	
	public void enterDetails(HotelStandardInfo setupDetails) throws Exception {
/*
		// delete existing hotel with the same name

		// waitForFrameAndSwitchToIt("frame_standard_info_id", 10);
		getElement("radiobutton_delete_hotel_id").click();
		getElement("inputfield_hotel_name_xpath").setText(setupDetails.getHotelName());
		getElement("button_hotel_name_lookup_xpath").click();
		waitElementLoad(2);
		// DriverFactory.getInstance().getDriver().switchTo().frame("lookup");
		boolean flag = getElement("button_hotel_name_1st_record_2_xpath").isElementAvailable();// DriverFactory.getInstance().getDriver().findElement(By.xpath("/html/body/div[4]/div/div/div[2]/div[3]/div/div/div[1]/ul/li[1]")).isDisplayed();//
																								// getElement("button_hotel_name_1st_record_2_xpath").isElementAvailable();

		// getElementVisibility("button_hotel_name_1st_record_2_xpath", 1);
		if (flag) {

			logger.info("Hotel found on " + setupDetails.getHotelName() + " name ");
			getElement("button_hotel_name_1st_record_2_xpath").click();
			getElementVisibility("button_save_id", 1);
			getElement("button_save_id").click();

		} else {
			Thread.sleep(3000);
			logger.info("No Hotel found on " + setupDetails.getHotelName() + " name ");
			getElement("button_hotel_namelookup_close_xpath").click();
		}
		// create new hotel

		// getElement("radiobutton_create_new_hotel_xpath").click();
		waitElementLoad(5);
		//switchToDefaultFrame();
		// DriverFactory.getInstance().getDriver().switchTo().defaultContent();

		//switchToFrame("frame_standard_info_id");
		// DriverFactory.getInstance().getDriver().switchTo().frame("hotelStandardInfoIframe");
*/
		getElement("radiobutton_create_new_hotel_id").click();
		// DriverFactory.getInstance().getDriver().findElement(By.id("CREATE")).click();
		Random rand = new Random();
		int u = rand.nextInt(1000);

		// getElement("inputfield_hotel_id_xpath").setText(String.valueOf(u));
		DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='standardInfoSec']/div/div[2]/div/div[1]/div[1]/div[1]/div[1]/input")).sendKeys(String.valueOf(u));
		// getElement("inputfield_hotel_name_xpath").setText(setupDetails.getHotelName());
		DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='standardInfoSec']/div/div[2]/div/div[1]/div[1]/div[2]/div[1]/input")).sendKeys(setupDetails.getHotelName());
		// getElement("inputfield_hotel_group_xpath").setText(setupDetails.getHotelGroup());
		DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='standardInfoSec']/div/div[2]/div/div[1]/div[1]/div[3]/div[1]/input")).sendKeys(setupDetails.getHotelGroup());
		// getElement("button_hotel_group_lookup_xpath").click();
		DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='standardInfoSec']/div/div[2]/div/div[1]/div[1]/div[3]/div[2]/a/i")).click();

		
		
		
		
		
		boolean flag2 = getElement("button_hotel_group_1st_record_xpath").isElementAvailable(); // DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='hotelGroupMasterRec']/div[1]/ul/li")).isDisplayed();

		if (flag2) {
				
			DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='hotelGroupMasterRec']/div[1]/ul/li")).click();
			//getElement("button_hotel_group_1st_record_xpath").click();
		} else {
			// DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='hotelGroupModal']/div/div/div[1]/button")).click();
			getElement("button_hotel_group_lookup_close_xpath").click();
			logger.info("hotel group " + setupDetails.getHotelGroup() + " is not availbile then close lookup screen ");
		}

		// Set Star category
		waitElementLoad(6);
		getElement("inputfield_star_category_xpath").setText(setupDetails.getStarCategory());
		getElement("button_star_category_lookup_xpath").click();
		
		WebElement starCatEle = DriverFactory.getInstance().getDriver().findElement(By.id("hotelStarCategoryRec"));
		List<WebElement> starList = starCatEle.findElements(By.tagName("a"));
		
		
		//boolean flag3 = DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='hotelStarCategoryRec']/div[1]/ul/li/a")).isDisplayed(); // getelementPresence(getElement("button_star_category_1st_record_xpath"))

		if (starList.size()>=1) {
			
			DriverFactory.getInstance().getDriver().findElement(By.partialLinkText(setupDetails.getStarCategory())).click();
			//DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='hotelStarCategoryRec']/div[1]/ul/li/a")).click();
		} else {
			getElement("button_star_category_lookup_close_xpath").click();
			
			logger.info("Star_category " + setupDetails.getStarCategory() + " is not availbile then close lookup screen ");
		}

		// Set Total number of Rooms
		// switchToDefaultFrame();
		DriverFactory.getInstance().getDriver().switchTo().defaultContent();

		// switchToFrame("frame_standard_info_id");
		DriverFactory.getInstance().getDriver().switchTo().frame("hotelStandardInfoIframe");

		DriverFactory.getInstance().getDriver().findElement(By.xpath(".//*[@id='standardInfoSec']/div/div[2]/div/div[1]/div[1]/div[5]/div/input")).sendKeys(setupDetails.getNumberOfRooms());
		// getElement("inputfield_total_number_of_rooms_xpath").setText(setupDetails.getNumberOfRooms());
		waitElementLoad(5);

		// pms hotel
		if (setupDetails.getPmsHotel().equals("yes")) {
			getElement("radiobutton_pms_hotel_yes_xpath").click();
		} else {
			getElement("radiobutton_pms_hotel_no_xpath").click();
		}

		// children allowed
		if (setupDetails.getChildrenAllowed().equals("yes"))
			getElement("radiobutton_children_allowed_yes_xpath").click();
		else
			getElement("radiobutton_children_allowed_no_xpath").click();

		DriverFactory.getInstance().getDriver().switchTo().defaultContent();
		DriverFactory.getInstance().getDriver().switchTo().frame("hotelStandardInfoIframe");
		// multiple child groups
		if (setupDetails.getMultipleChildAgeGroup().equals("yes")) {
			getElement("checkbox_multiple_child_name").click();
			getElement("button_multiple_child_age_group_xpath").click();
			waitElementLoad(1);
			String[] ageGrp = setupDetails.getAges().split("/");

			for (int i = 1; i <= ageGrp.length; i++) {
				getElement("inputfield_multi_age_name_xpath").changeRefAndGetElement(getElement("inputfield_multi_age_name_xpath").getRef().replace("replace", String.valueOf(i))).setText("group" + i);
				((ComboBox) getElement("combobox_multi_age_from_xpath").changeRefAndGetElement(getElement("combobox_multi_age_from_xpath").getRef().replace("replace", String.valueOf(i)))).selectOptionByValue(ageGrp[(i - 1)].split("-")[0]);
				((ComboBox) getElement("combobox_multi_age_to_xpath").changeRefAndGetElement(getElement("combobox_multi_age_to_xpath").getRef().replace("replace", String.valueOf(i)))).selectOptionByValue(ageGrp[(i - 1)].split("-")[1]);

			}
			getElement("button_multi_age_save_xpath").click();
		}
		waitElementLoad(3);// implicit wait
		switchToDefaultFrame();
		switchToFrame("frame_standard_info_id");

		getElement("inputfield_supplier_name_xpath").setText(setupDetails.getSupplierName());
		if (getElementIsClickable("button_supplier_name_lookup_xpath", 1)) {
			getElement("button_supplier_name_lookup_xpath").click();
			if (getElementIsClickable("button_supplier_name_1st_record_xpath", 2)) {
				getElement("button_supplier_name_1st_record_xpath").click();
			} else {
				getElement("button_supplier_lookup_close_xpath").click();
				logger.info("supplier " + setupDetails.getStarCategory() + " is not availbile then close lookup screen ");

			}
		}
		switchToDefaultFrame();
		switchToFrame("frame_standard_info_id");
		// is hotel active
		if (setupDetails.getIsHotelActive().equals("yes"))
			getElements("radiobutton_is_hotel_active_name").get(0).click();
		else
			getElements("radiobutton_is_hotel_active_name").get(1).click();

		// featured hotel
		if (setupDetails.getFeaturedHotel().equals("yes"))
			getElements("radionbutton_featured_hotel_name").get(0).click();
		else
			getElements("radionbutton_featured_hotel_name").get(1).click();

		// transfer included
		if (setupDetails.getTransferincluded().equals("yes"))
			getElements("radiobutton_transfer_included_name").get(0).click();
		else
			getElements("radiobutton_transfer_included_name").get(1).click();

		// rate contract
		if (setupDetails.getRateContract().equals("per room"))
			getElements("radiobutton_rate_contract_name").get(0).click();
		else
			getElements("radiobutton_rate_contract_name").get(1).click();

		// rates setup by
		if (setupDetails.getRatesSetupBy().equals("net rate"))
			getElements("radiobutton_rates_setup_by_name").get(0).click();
		else
			getElements("radiobutton_rates_setup_by_name").get(1).click();

		// rates by
		if (setupDetails.getRatesBy().equals("per night"))
			getElements("radiobutton_rates_by_name").get(0).click();
		else
			getElements("radiobutton_rates_by_name").get(1).click();

		// chekin date
		if (setupDetails.getApplyCancellationPolicyBy().equals("by checkin date"))
			getElements("radiobutton_apply_cancellation_policy_name").get(0).click();
		else
			getElements("radiobutton_apply_cancellation_policy_name").get(1).click();

		// partner type deselect
		getElements("checkbox_partner_type_b2b_name").get(0).click();
		getElements("checkbox_partner_type_b2b_name").get(1).click();
		getElements("checkbox_partner_type_b2b_name").get(2).click();

		// partner type
		if (setupDetails.getPartnerType().equals("all")) {
			getElements("checkbox_partner_type_b2b_name").get(0).click();
			getElements("checkbox_partner_type_b2b_name").get(1).click();
			getElements("checkbox_partner_type_b2b_name").get(2).click();
		} else if (setupDetails.getPartnerType().equals("direct")) {
			getElements("checkbox_partner_type_b2b_name").get(0).click();
		} else if (setupDetails.getPartnerType().equals("b2b")) {
			getElements("checkbox_partner_type_b2b_name").get(1).click();
		} else if (setupDetails.getPartnerType().equals("affiliate")) {
			getElements("checkbox_partner_type_b2b_name").get(2).click();
		}

		// shopping cart deselect
		getElements("checkbox_booking_type_name").get(0).click();
		getElements("checkbox_booking_type_name").get(1).click();
		getElements("checkbox_booking_type_name").get(2).click();
		getElements("checkbox_booking_type_name").get(3).click();

		// shopping cart
		if (setupDetails.getShoppingCart().equals("all")) {
			getElements("checkbox_booking_type_name").get(0).click();
			getElements("checkbox_booking_type_name").get(1).click();
			getElements("checkbox_booking_type_name").get(2).click();
			getElements("checkbox_booking_type_name").get(3).click();
		} else if (setupDetails.getShoppingCart().equals("mobile")) {
			getElements("checkbox_booking_type_name").get(0).click();
		} else if (setupDetails.getShoppingCart().equals("web")) {
			getElements("checkbox_booking_type_name").get(1).click();
		} else if (setupDetails.getShoppingCart().equals("cc")) {
			getElements("checkbox_booking_type_name").get(2).click();
		} else if (setupDetails.getShoppingCart().equals("web services")) {
			getElements("checkbox_booking_type_name").get(3).click();
		}

		// dynamic package deselect
		getElements("checkbox_booking_type_name").get(4).click();
		getElements("checkbox_booking_type_name").get(5).click();
		getElements("checkbox_booking_type_name").get(6).click();
		getElements("checkbox_booking_type_name").get(7).click();

		// dynamic package
		if (setupDetails.getDynamicPackage().equals("all")) {
			getElements("checkbox_booking_type_name").get(4).click();
			getElements("checkbox_booking_type_name").get(5).click();
			getElements("checkbox_booking_type_name").get(6).click();
			getElements("checkbox_booking_type_name").get(7).click();
		} else if (setupDetails.getDynamicPackage().equals("mobile")) {
			getElements("checkbox_booking_type_name").get(4).click();
		} else if (setupDetails.getDynamicPackage().equals("web")) {
			getElements("checkbox_booking_type_name").get(5).click();
		} else if (setupDetails.getDynamicPackage().equals("cc")) {
			getElements("checkbox_booking_type_name").get(6).click();
		} else if (setupDetails.getDynamicPackage().equals("web services")) {
			getElements("checkbox_booking_type_name").get(7).click();
		}

		// fixed package deselect
		getElements("checkbox_booking_type_name").get(8).click();
		getElements("checkbox_booking_type_name").get(9).click();
		getElements("checkbox_booking_type_name").get(10).click();
		getElements("checkbox_booking_type_name").get(11).click();

		// fixed package
		if (setupDetails.getDynamicPackage().equals("all")) {
			getElements("checkbox_booking_type_name").get(8).click();
			getElements("checkbox_booking_type_name").get(9).click();
			getElements("checkbox_booking_type_name").get(10).click();
			getElements("checkbox_booking_type_name").get(11).click();
		} else if (setupDetails.getDynamicPackage().equals("mobile")) {
			getElements("checkbox_booking_type_name").get(8).click();
		} else if (setupDetails.getDynamicPackage().equals("web")) {
			getElements("checkbox_booking_type_name").get(9).click();
		} else if (setupDetails.getDynamicPackage().equals("cc")) {
			getElements("checkbox_booking_type_name").get(10).click();
		} else if (setupDetails.getDynamicPackage().equals("web services")) {
			getElements("checkbox_booking_type_name").get(11).click();
		}

		// enter the Hotel Address
		getElement("inputfield_address1_xpath").setText(setupDetails.getAddress1());
		getElement("inputfield_address2_xpath").setText(setupDetails.getAddress2());
		getElement("inputfield_country_xpath").setText(setupDetails.getCountry());

		getElement("button_country_xpath").click();
		waitElementLoad(1);
		getElement("button_country_1st_reocrd_xpath").click();
		waitElementLoad(1);
		getElement("inputfield_city_xpath").setText(setupDetails.getCity());
		waitElementLoad(1);
		// getElementIsClickable("button_city_xpath", 10);
		getElement("button_city_xpath").click();
		waitElementLoad(3);
		getElement("button_city_1st_reocrd_xpath").click();
		getElement("inputfield_zipcode_xpath").setText(setupDetails.getZipcode());

		// contact details
		if (getElementVisibility("inputfield_contact_name_xpath", 5)) {
			getElement("inputfield_contact_name_xpath").setText(setupDetails.getContactname());
			getElement("inputfield_email_xpath").setText(setupDetails.getEmail());
			waitElementLoad(3);
			getElement("button_contact_add_xpath").click();
		} else {
			Assert.fail("can't add the Contact section");
		}
		// getElementIsClickable("button_contact_add_xpath", 10);

	}

	public boolean isSaveTheDetails() throws Exception {

		getElement("button_save_id").click();
		waitElementLoad(5);

		if (getElementVisibility("button_hotel_already_available_xpath", 10)) {
			getElement("button_hotel_already_available_xpath").click();
			logger.info("hotel_already_available for this hotel id");
		} else {
			getElement("button_save_id").click();
		}

		boolean flag = DriverFactory.getInstance().getDriver().findElement(By.xpath("//*[@id='standard']/div/div[1]")).getText().equalsIgnoreCase("Data Updated Sucessfully");
		
		if(flag){
			return true;
		}else{
			return false;
		}

	}

	public CC_HotelSetup_Contract proccedToContractPage() throws Exception {
		CC_HotelSetup_Contract contractObj = new CC_HotelSetup_Contract();

		getElement("button_contract_xpath").click();
		if (contractObj.checkAvailability()) {
			return contractObj;
		} else {
			return null;
		}
	}
	
	public List<WebElement> getElementList(String locator , String tag){
		
		WebElement ele = DriverFactory.getInstance().getDriver().findElement(By.className(locator));
		List<WebElement> eleList = ele.findElements(By.tagName(tag));
		
		return eleList;
		
	}
	
	
}
