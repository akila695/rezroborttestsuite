package com.rezgateway.automation.pages.hotelSetup;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.rezgateway.automation.pojo.JqueryDropDown;
import com.rezgateway.automation.utill.DriverFactory;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelRoomAndInventoryInfo;
import com.rezgateway.automation.Objects.HotelSetupDetails;

@SuppressWarnings("unused")
public class CC_HotelSetup_Contract_Popup_ContractDetails extends PageObject {

	public boolean checkAvailability() {
		try {
			return getElementIsClickable("inputfield_hc_period_from_name", 60);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	public ArrayList<String> enterDetails(HotelSetupDetails setupDetails, String hotelName) throws Exception {
		ArrayList<HotelContractInfo> contractList = new ArrayList<>();
		HotelRoomAndInventoryInfo room = new HotelRoomAndInventoryInfo();
		int numberOfRooms = 0;

		boolean allotment = false;
		boolean freeSell = false;
		boolean onReq = false;

		for (int i = 0; i < setupDetails.getInventoryInfo().size(); i++) {
			if (setupDetails.getInventoryInfo().get(i).getHotelName().equals(hotelName)) {
				if (setupDetails.getInventoryInfo().get(i).getInventoryTypeAllotment().equals("yes"))
					allotment = true;
				if (setupDetails.getInventoryInfo().get(i).getInventoryTypeFreeSell().equals("yes"))
					freeSell = true;
				if (setupDetails.getInventoryInfo().get(i).getInventoryTypeOnReq().equals("yes"))
					onReq = true;

				numberOfRooms++;
			}
		}

		for (int i = 0; i < setupDetails.getContractinfo().size(); i++) {
			if (setupDetails.getContractinfo().get(i).getHotelName().equals(hotelName)) {
				contractList.add(setupDetails.getContractinfo().get(i));
			}
		}

		executejavascript("document.getElementsByName('start')[0].value='" + contractList.get(0).getPeriodFrom() + "';");
		executejavascript("document.getElementsByName('end')[0].value='" + contractList.get(0).getPeriodTo() + "';");

		// select all the allotment types
		if (allotment) {
			System.out.println(getElements("checkbox_hc_inventory_options_name").size());
			for (int i = 0; i < getElements("checkbox_hc_inventory_options_name").size(); i++) {
				try {
					getElements("checkbox_hc_inventory_options_name").get(i).click();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}

		for (int i = 0; i < contractList.size(); i++) {
			getElement("button_hc_inventory_type_xpath").click();
			// select inventory type
			for (int j = 1; j < 4; j++) {
				Thread.sleep(2000);

				if (getElement("label_hc_select_inventory_xpath").changeRefAndGetElement(getElement("label_hc_select_inventory_xpath").getRef().replace("replace", String.valueOf(j))).getText()
						.equalsIgnoreCase(contractList.get(i).getSelectAllotment())) {
					getElement("radiobutton_hc_select_inventory_xpath").changeRefAndGetElement(getElement("radiobutton_hc_select_inventory_xpath").getRef().replace("replace", String.valueOf(j))).click();
				}
			}

			Thread.sleep(3000);
			getElement("button_hc_room_type_xpath").click();
			// select room type
			for (int j = 1; j < (numberOfRooms + 1); j++) {
				if (getElement("label_hc_room_type_xpath").changeRefAndGetElement(getElement("label_hc_room_type_xpath").getRef().replace("replace", String.valueOf(j))).getText().equalsIgnoreCase(contractList.get(i).getSelectRoom())) {
					getElement("checkbox_room_type_xpath").changeRefAndGetElement(getElement("checkbox_room_type_xpath").getRef().replace("replace", String.valueOf(j))).click();
				}
			}

			getElement("button_hc_create_xpath").click();

		}

		System.out.println("Hotel contract - Room creation completed");

		for (int i = 0; i < contractList.size(); i++) {
			getElementVisibility("inputfield_rc_contract_name_xpath", 10);
			Thread.sleep(3000);
			getElement("inputfield_rc_contract_name_xpath").clear();
			getElement("inputfield_rc_contract_name_xpath").setText(contractList.get(i).getContractname());
			getElement("inputfield_rc_currency_xpath").clear();
			getElement("inputfield_rc_currency_xpath").setText(contractList.get(i).getCurrency());
			Thread.sleep(3000);
			getElement("inputfield_rc_currency_xpath").setText(Keys.ARROW_DOWN);
			getElement("inputfield_rc_currency_xpath").setText(Keys.TAB);

			String[] region = contractList.get(i).getApllicableRegion().trim().split("/");
			String[] country = contractList.get(i).getApplicalbeCountry().trim().split("/");
			String[] excluding = contractList.get(i).getExcludingCountry().trim().split("/");
			if (contractList.get(i).getRateBy().equals("all")) {
				getElements("radiobutton_rc_rateby_name").get(2).click();

				for (int j = 0; j < excluding.length; j++) {
					getElement("inputfield_rc_excluding_country_xpath").setText(excluding[j]);
					Thread.sleep(3000);
					((JqueryDropDown) getElement("button_automa_complete_classname")).selectOptionByValue(excluding[j]);
					getElement("inputfield_rc_excluding_country_xpath").setText(Keys.ARROW_DOWN);
					getElement("inputfield_rc_excluding_country_xpath").setText(Keys.TAB);
				}
			} else {
				getElements("radiobutton_rc_rateby_name").get(3).click();

				for (int j = 0; j < region.length; j++) {
					if (!region[j].equals("n")) {
						getElement("inputfield_rc_applicable_region_xpath").clear();
						getElement("inputfield_rc_applicable_region_xpath").setText(region[j]);

						Thread.sleep(5000);
						/*
						 * System.out.println(getElements("button_automa_complete_classname").size()); try {
						 * ((JqueryDropDown)getElementsUI("button_automa_complete_classname").get(0)).selectOptionByValue(excluding[j]); } catch (Exception e) { // TODO: handle exception
						 * System.out.println(e); }
						 * 
						 * try { ((JqueryDropDown)getElementsUI("button_automa_complete_classname").get(1)).selectOptionByValue(excluding[j]); } catch (Exception e) { // TODO: handle exception
						 * System.out.println(e); }
						 */

						getElement("inputfield_rc_applicable_region_xpath").setText(Keys.ARROW_DOWN);
						waitElementLoad(2);
						getElement("inputfield_rc_applicable_region_xpath").setText(Keys.ENTER);
						waitElementLoad(2);
						System.out.println("region is selected");
					}
				}

				for (int j = 0; j < country.length; j++) {
					if (!country[j].equals("n")) {
						getElement("inputfield_rc_applicable_country_xpath").clear();
						getElement("inputfield_rc_applicable_country_xpath").setText(country[j]);
						Thread.sleep(5000);
						getElement("inputfield_rc_applicable_country_xpath").setText(Keys.ARROW_DOWN);
						getElement("inputfield_rc_applicable_country_xpath").setText(Keys.TAB);
						waitElementLoad(2);
					}
				}

				for (int j = 0; j < excluding.length; j++) {
					if (!excluding[j].equals("n")) {
						getElement("inputfield_rc_excluding_country_xpath").clear();
						getElement("inputfield_rc_excluding_country_xpath").setText(excluding[j]);
						waitElementLoad(2);
						getElement("inputfield_rc_excluding_country_xpath").setText(Keys.ARROW_DOWN);
						getElement("inputfield_rc_excluding_country_xpath").setText(Keys.TAB);
						waitElementLoad(2);
					}
				}
			}

			System.out.println(getElements("checkbox_rc_partner_type_name").size());

			if (contractList.get(i).getPartnerTypeDirect().equals("no"))
				getElements("checkbox_rc_partner_type_name").get(4).click();
			if (contractList.get(i).getPartnerTypeB2B().equals("no"))
				getElements("checkbox_rc_partner_type_name").get(5).click();
			if (contractList.get(i).getPartnerTypeAffiliate().equals("no"))
				getElements("checkbox_rc_partner_type_name").get(6).click();

			if (contractList.get(i).getBookingChannelMobile().equals("no"))
				getElements("checkbox_rc_booking_channel_name").get(1).click();
			if (contractList.get(i).getBookingChannelWeb().equals("no"))
				getElements("checkbox_rc_booking_channel_name").get(2).click();
			if (contractList.get(i).getBookingChannelCC().equals("no"))
				getElements("checkbox_rc_booking_channel_name").get(3).click();
			if (contractList.get(i).getBookingChannelWebService().equals("no"))
				getElements("checkbox_rc_booking_channel_name").get(4).click();
			if (contractList.get(i).getBookingTypeShoppingCart().equals("no"))
				getElements("checkbox_rc_booking_type_name").get(13).click();
			if (contractList.get(i).getBookingTypeDP().equals("no"))
				getElements("checkbox_rc_booking_type_name").get(14).click();
			if (contractList.get(i).getBookingTypeFP().equals("no"))
				getElements("checkbox_rc_booking_type_name").get(15).click();

			waitElementLoad(2);
			getElementIsClickable("button_rc_add_xpath", 10);
			getElement("button_rc_add_xpath").click();

		}

		// getElementIsClickable("button_save_xpath", 10);
		getElement("button_save_xpath").click();
		waitElementLoad(5);
		ArrayList<String> contracts = new ArrayList<>();
		for (int i = 0; i < getElements("label_obj_val_classname").size(); i++) {
			try {
				if (!getElements("label_obj_val_classname").get(i).getAttribute("objval").equals("null")) {
					contracts.add(getElements("label_obj_val_classname").get(i).getAttribute("objval"));
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return contracts;
	}

	public CC_HotelSetup_Contract_Popup_AssignRoomCombinations proccedToAssingRoomCombiination() throws Exception {

		CC_HotelSetup_Contract_Popup_AssignRoomCombinations assignRoomCombination = new CC_HotelSetup_Contract_Popup_AssignRoomCombinations();

		getElement("button_LinkToassignRoomCombination_xpath").click();
		
		if(getelementAvailability("button_doYouWantSaveNO_xpath"))
			getElement("button_doYouWantSaveNO_xpath").click();
		
		waitElementLoad(3);
		if (assignRoomCombination.checkAvailability()) {
			return assignRoomCombination;
		} else {
			return null;
		}
	}

	public CC_HotelSetup_Contract_Popup_BillingInfo proccedToBillingInfo() throws Exception {

		CC_HotelSetup_Contract_Popup_BillingInfo billingInfo = new CC_HotelSetup_Contract_Popup_BillingInfo();
		
		getElement("button_LinkToBillingInfo_xpath").click();
		
		if(getelementAvailability("button_doYouWantSaveNO_xpath"))
			getElement("button_doYouWantSaveNO_xpath").click();
		
		waitElementLoad(3);
		if (billingInfo.checkAvaialbilty()) {
			return billingInfo;
		} else {
			return null;
		}

	}

	public CC_HotelSetup_Contract_Popup_TaxDetails proccedToTaxDetails() throws Exception {
		
		CC_HotelSetup_Contract_Popup_TaxDetails taxDetails =  new CC_HotelSetup_Contract_Popup_TaxDetails();
		getElement("button_LinkTohotelTaxDetails_xpath").click();
		
		if(getelementAvailability("button_doYouWantSaveNO_xpath"))
			getElement("button_doYouWantSaveNO_xpath").click();
		if(taxDetails.checkAvailability()){
			return taxDetails;	
		}else{
			return null;
		}
	}

}
