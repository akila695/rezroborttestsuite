package com.rezgateway.automation.pages.hotelSetup;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.Objects.HotelSetupDetails;

public class CC_HotelSetup_Contract_Popup_BillingInfo extends PageObject{
	
	public boolean checkAvaialbilty()
	{
		try {
			return getElementVisibility("checkbox_rate_contract_xpath" , 60);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	
	public void enterDetails(HotelSetupDetails setupDetails, String hotelName) throws Exception
	{
		for(int i = 0 ; i < setupDetails.getBillingInfo().size() ; i++)
		{
			if(hotelName.equals(setupDetails.getBillingInfo().get(i).getHotelName()))
			{
				getElement("inputfield_bank_xpath").setText(setupDetails.getBillingInfo().get(i).getBankName());
				getElement("inputfield_branch_xpath").setText(setupDetails.getBillingInfo().get(i).getBranch());
				getElement("inputfield_account_number_xpath").setText(setupDetails.getBillingInfo().get(i).getAccountNumber());
				getElement("inputfield_swift_code_xpath").setText(setupDetails.getBillingInfo().get(i).getSwiftCode());
				
				getElement("button_add_classname").click();
				Thread.sleep(2000);
				getElement("button_save_classname").click();
			}
		}
	}
}
