package com.rezgateway.automation.pages.common;

import com.rezgateway.automation.pojo.InputFiled;
import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.enums.UserType;

public class CC_Com_LoginPage extends PageObject {


public CC_Com_LoginPage() {
	//getElement("input_userID_id").checkElementVisisbilityWithException(120, "LoginPage NotLoaded");
	
}
	
public void typeUserName(String Username) throws Exception {
	//((InputFiled)getElement("input_userID_id")).clearInput();
	switchToDefaultFrame();
	System.out.println("Ref type id " + getElement("input_userID_id").getRef());
	System.out.println("Is this ref type is availbile "+ getElement("input_userID_id").isElementAvailable());
	getElement("input_userID_id").setText(Username);
	
}
public void typePassword(String Password) throws Exception {
	((InputFiled)getElement("input_password_id")).clearInput();
	getElement("input_password_id").setText(Password);
}

public void loginSubmit() throws Exception {
	try {
		getElementVisibility("btn_loginbutton_id", 5);
		getElement("btn_loginbutton_id").click();
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println("login error -:" + e);
		try {
			getElement("btn_loginbutton_id").click();
		} catch (Exception e2) {
			// TODO: handle exception
		}
	}
	
	
}

public CC_Com_HomePage loginWithValidCredentials(String Username,String Password) throws Exception {
	
	typeUserName(Username);
	typePassword(Password);
	Thread.sleep(4000);
//	executejavascript("return validate('formH');");
	loginSubmit();
	CC_Com_HomePage home=new CC_Com_HomePage();
	return home;
	
	
}

@SuppressWarnings("static-access")
public CC_Com_HomePage loginAs(UserType UserType,String Username,String Password) throws Exception {
	
	typeUserName(Username);
	typePassword(Password);
	Thread.sleep(4000);
//	executejavascript("return validate('formH');");
	loginSubmit();
	
	
	
	if (UserType == UserType.INTERNAL)
	return new CC_Com_HomePage();
	else if (UserType == UserType.ADMIN)
	return new CC_Com_Admin_Home();
	else 
	return new CC_Com_HomePage();

	//return home;
	
	
}
public String getPortalHeader() throws Exception {
	
	return getElement("txt_PortalHeader_class").getText();
	
}

public boolean isLoginPageAvailable()
{
	switchToDefaultFrame();
	return super.isPageAvailable("input_userID_id");
}

public String getinvalidLoginError(String CorrectUserName, String incorrectPassword) throws Exception {
	
	typeUserName(CorrectUserName);
	typePassword(incorrectPassword);
	loginSubmit();
	addobjects();
	return getElement("div_invalidLoginError_id").getText();
	
	
	
}

/*public static void main(String[] args) throws Exception {
	
	DriverFactory.getInstance().initializeDriver();
	DriverFactory.getInstance().getDriver().get("http://web.bo.base.col.rezg.net/rezproduction/admin/common/LoginPage.do");
	
	CC_Com_LoginPage login=new CC_Com_LoginPage();
	login.getPortalHeader();
	//System.out.println(login.getinvalidLoginError("KavithaP", "kavitha"));
	//DriverFactory.getInstance().getDriver().get("http://dev3.rezg.net/omanairholidays/admin/common/LoginPage.do");

	CC_Com_Admin_Home adminHome = (CC_Com_Admin_Home) login.loginAs(UserType.ADMIN,"rezgadmin","rezgadmin123");
	CC_Com_Admin_ConfigurationPage configpage = adminHome.getConfigurationScreen();
	
	System.out.println(configpage.getConfigProperties());
}*/
	



}
