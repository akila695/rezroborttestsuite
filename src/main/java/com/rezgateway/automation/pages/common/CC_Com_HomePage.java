package com.rezgateway.automation.pages.common;

import com.rezgateway.automation.core.PageObject;
import com.rezgateway.automation.pages.CC.common.V11_CC_Com_HeaderPage;


public class CC_Com_HomePage extends PageObject {
	
	public boolean isPageAvailable() {
		// TODO Auto-generated method stub
		return super.isPageAvailable("lnk_Operations_id");
	}
	
	public V11_CC_Com_HeaderPage navigateToContractMenu() throws Exception{
		 
		getElement("lnk_Contracting_id").click();
		V11_CC_Com_HeaderPage operationHeaderPage = new V11_CC_Com_HeaderPage();
		
		return operationHeaderPage;
		
	}
	
	
	public void performHotelSarch() throws Exception
	{
		getElement("button_bec_search_for_hotels_id").click();
	}
	

}
