package com.rezgateway.automation.rezrobotTestSuite;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

import com.rezgateway.automation.Objects.HotelContractInfo;
import com.rezgateway.automation.Objects.HotelSetupDetails;
import com.rezgateway.automation.Objects.HotelStandardInfo;
import com.rezgateway.automation.core.RunnerBase;
import com.rezgateway.automation.pages.CC.common.V11_CC_Com_HeaderPage;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkupConfiguration_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkupHotelSelection_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkupUpdateInfo_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkupView_Page;
import com.rezgateway.automation.pages.backoffice.V11_CC_Hotel_ProfitMarkup_Page;
import com.rezgateway.automation.pages.common.CC_Com_HomePage;
import com.rezgateway.automation.pages.common.CC_Com_LoginPage;
import com.rezgateway.automation.processor.HotelSetupExcelHandler;
import com.rezgateway.automation.utill.DriverFactory;

public class HotelProfitMarkupTest extends RunnerBase {

	private HashMap<String, String> portalInfo = new HashMap<>();
	private WebDriver driver = null;

	private CC_Com_HomePage home;
	private V11_CC_Com_HeaderPage contractMenu;
	private V11_CC_Hotel_ProfitMarkup_Page markupPage;
	private V11_CC_Hotel_ProfitMarkupConfiguration_Page markupConfigPage;
	private V11_CC_Hotel_ProfitMarkupHotelSelection_Page markupHotelSelectionPage;
	private V11_CC_Hotel_ProfitMarkupSelectByPeriod_Page markupSelectByPeriod;
	private V11_CC_Hotel_ProfitMarkupUpdateInfo_Page markupUpdateInfo;
	private V11_CC_Hotel_ProfitMarkupView_Page markUpViewPage;

	private HotelSetupDetails setupDetails;
	private HotelSetupExcelHandler excelHandler = new HotelSetupExcelHandler();
	private HotelStandardInfo standardDetails;
	private HotelContractInfo contractInfo;

	@BeforeClass
	public void testDriverInitial() throws Exception {

		portalInfo = readProperty(System.getProperty("user.dir") + File.separator + "Rezrobot_Details/Common/portalSpecificData/portalInfo.properties");
		initiateDriver();
		driver = DriverFactory.getInstance().getDriver();
		driver.manage().deleteAllCookies();

	}

	@Parameters({ "UserName", "Password" })
	@Test
	public void loginTest(String un, String pw) throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Login Test");
		result.setAttribute("Expected", "Should be logged in Successfully");
		/******************************************************************/

		driver.get(portalInfo.get("PortalUrlCC").concat(portalInfo.get("login")));
		CC_Com_LoginPage login = new CC_Com_LoginPage();
		home = login.loginWithValidCredentials(un, pw);
		assertTrue(home.isPageAvailable(), "Login Page Test");

	}

	@Test(dependsOnMethods = { "loginTest" })
	public void hotelProfitMarkupSetupMenuLinkAvailability() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Check the Hotel Profit markup Setup Menu Link Availability ");
		result.setAttribute("Expected", " Should Display Hotel Profit markup Setup Menu Link ");
		/******************************************************************/

		contractMenu = home.navigateToContractMenu();
		if (contractMenu.isHotelProfitMarginSetupLinkAvailabile().getText().equalsIgnoreCase("Hotel Profit Margin")) {
			result.setAttribute("Actual", "Hotel Profit margin setup link is available in the Hotel Menu");
		} else {
			result.setAttribute("Actual", "Not available in the Header page");
			Assert.fail("Not available in the Header page");
		}
	}

	@Test(dependsOnMethods = { "hotelProfitMarkupSetupMenuLinkAvailability" })
	public void setDetailsTest() throws Exception {

		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Enter the Details in the Screen");
		result.setAttribute("Expected", "System should enter the Detail without any Interruption");
		/******************************************************************/

		setupDetails = excelHandler.getObject(portalInfo.get("hotelSetupExcel"));
		standardDetails = setupDetails.getStandardInfo().get(0);
		contractInfo = setupDetails.getContractinfo().get(0);

		markupPage = contractMenu.proccedToProfitMarkUpPage();

		if (markupPage.isPageAvailabile().isDisplayed()) {

			markupHotelSelectionPage = markupPage.proccedToHotelSelectionPage();
			markupHotelSelectionPage.selectCountry(standardDetails.getCountry());
			markupHotelSelectionPage.selectCity(standardDetails.getCity());
			markupHotelSelectionPage.selecSingleHotel(contractInfo);

			markupConfigPage = markupHotelSelectionPage.proccedToConfigurationPage();
			markupConfigPage.selectB2CPartnerType();
			markupConfigPage.selectBookingTypeAll();
			markupConfigPage.selectBookingChannelAll();
			markupConfigPage.selectOverwriteSpecificMarginOption("No");

			markupSelectByPeriod = markupConfigPage.proccedToMarkupSelectByPeriodPage();
			markupSelectByPeriod.setDatePeriod(contractInfo);

			markupUpdateInfo = markupSelectByPeriod.addingDatePeriodToUpdateInfoPage();
			try {
				if((markupUpdateInfo.availablePeriod().getText().contains(contractInfo.getPeriodFrom()))&&markupUpdateInfo.availablePeriod().getText().contains(contractInfo.getPeriodFrom())){
						markupUpdateInfo.selectMarginType("10");
						markupUpdateInfo.selectAdditionalAdultMarkup("10");
						markupUpdateInfo.selectChildMarkup("10");
					if (markupUpdateInfo.saveTheDetails().getText().equals("Successfully Saved")) {
						result.setAttribute("Actual", "Saved Successfuly");
					} else {
						result.setAttribute("Actual", " Not saved : Due to " + markupUpdateInfo.saveTheDetails().getText());
						Assert.fail(" Not saved : Due to " + markupUpdateInfo.saveTheDetails().getText());
					}
				}else{
					result.setAttribute("Actual", "date period is wrong  " +markupUpdateInfo.availablePeriod().getText());
					Assert.fail(" date period is wrong: Due to " + markupUpdateInfo.availablePeriod().getText());
				}
			
			} catch (Exception e) {
				result.setAttribute("Actual", " Fail  due to :  " + e);
				Assert.fail("Fail  due to : " + e);
			}

		} else {
			result.setAttribute("Actual", "Hotel Profit Markup serivice is down ");
			Assert.fail("Hotel Profit Markup serivice is down");
		}

	}
	
	
	@Test(dependsOnMethods = { "setDetailsTest" })
	public void testTheSaveData() throws Exception{
		
		/******************************************************************/
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", "Enter the Details in the Screen");
		result.setAttribute("Expected", "System should enter the Detail without any Interruption");
		/******************************************************************/

		
		markupHotelSelectionPage = markupPage.proccedToHotelSelectionPage();
		markupHotelSelectionPage.selectCountry(standardDetails.getCountry());
		markupHotelSelectionPage.selectCity(standardDetails.getCity());
		markupHotelSelectionPage.selecSingleHotel(contractInfo);

		markupConfigPage = markupHotelSelectionPage.proccedToConfigurationPage();
		markupConfigPage.selectB2CPartnerType();
		markupConfigPage.selectBookingTypeAll();
		markupConfigPage.selectBookingChannelAll();
		markupConfigPage.selectOverwriteSpecificMarginOption("No");

		markupSelectByPeriod = markupConfigPage.proccedToMarkupSelectByPeriodPage();
		markupSelectByPeriod.setDatePeriod(contractInfo);

		//select view Section
		markUpViewPage = markupSelectByPeriod.proccedToProfitMarkupViewPage();
		if(null == markUpViewPage.getViewGrid()){
			result.setAttribute("Actual", "Saved Record is not avilabile ");
			Assert.fail("Saved Record is not avilabile ");
		}else{
			result.setAttribute("Actual", "Saved Record is avilabile");
			// ${TODO}: implement set data into MarginViewGrid object after fix that not Display old record issue.
		}
		
	}
	

	@AfterClass
	public void tearDown() {
		//DriverFactory.getInstance().getDriver().quit();

	}

}
